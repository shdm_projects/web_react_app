import React, { Component } from 'react';
import Menu from '../../Elements/Menu/Menu';
import SmallMenu from '../../Elements/SmallMenu/SmallMenu';
import './index.css';



export default class Top extends Component{

	constructor(props){
		super(props);

		this.state = {
			data: props.data,
			config: props.config
		}


	}

	render(){

		var element = null;

		if(this.state.config.top_component === "small_menu"){
			element = <SmallMenu data={this.state.data} />;
		}else if(this.state.config.top_component === "big_menu"){
			element = <SmallMenu data={this.state.data} />;
		}else{
			element = <Menu data={this.state.data} />;
		}

		return (
			<div className="header_top">
				{element}
			</div>

		);

	}

}
