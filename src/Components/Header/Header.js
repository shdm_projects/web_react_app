import Top from './Top';
import './Header.css';
import Middle from './Middle';
import Bottom from './Bottom';
import React, { Component } from 'react';


export default class Header extends Component{

	constructor(props){
		super(props);

		this.state = {
			data: props.data,
			config: props.config
		}


	}

	render(props){

		var className = "header "+this.state.config.top_component;	

		const style = {

			"backgroundColor": '#'+this.state.config.gbcolor

		};

		return (
			<section className={className} style={style}>
		        <Top data={this.state.data} config={this.state.config}/>
		        <Middle data={this.state.data} config={this.state.config}/>
		        <Bottom data={this.state.data} config={this.state.config}/>  
	      	</section>
		);

	}

}