import Slider from '../../Elements/Slider/Slider';
import BigMenu from '../../Elements/BigMenu/BigMenu';
import React, { Component } from 'react';


export default class Middle extends Component{

	constructor(props){
		super(props);
		//console.log("Middle:");
		//console.log(props.data);

		this.state = {
			data: props.data,
			config: props.config
		}


	}

	render(){

		var element = null;

		if(this.state.config.top_component === "slider"){

			element = <Slider data={this.state.data} />;

		}else if(this.state.config.top_component === "big_menu"){

			element =
				<div className="big_menutopwrapper">
					<div className="big_menumask"></div>
					<div className="container">
						<BigMenu data={this.state.data} />
					</div>
				</div>;

		}else if(this.state.config.top_component === "small_menu"){

		}

		return (
			<div className="header_middle">

				{element}

			</div>
		);

	}

}
