class Helpers {

	static checkPropertiesExist(object, properties, show) {
		switch (typeof(properties)){
			case 'object':
				if (Array.isArray(properties))
				{
					for (var i = 0; i < properties.length; i++) {
						if(!object.hasOwnProperty(properties[i]))
							show ? console.warn('Property: ' + properties[i] + ' is not found') : null;
							return false;
					}
					return true;
				}
				else
				{	
					console.warn('Object property error: ' + properties);
					return false;
				}
			case 'string':
				show ? console.warn('Property: ' + properties + ' is not found') : null;
				return object.hasOwnProperty(properties);
			default:
				return false;
		}
	}
}

export default Helpers;