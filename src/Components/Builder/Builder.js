import React, {Component} from 'react';
import axios from 'axios';
import Header from '../../Components/Header/Header';
import Content from '../../Components/Content/Content';
import Footer from '../../Components/Footer/Footer';
import {withRouter} from 'react-router';
import Spinner from 'react-spinner';

import '../../assets/app.css';

import $ from 'jquery';

class Builder extends Component {

  //jquery scripts
   loadJquery(){

    $(document).ready(function() {

      console.log($('.navbar-toggle'));

        $('.navbar-toggle').click(function(event) {

            if (!$('.mainmenu-list').is(':visible')) {
                  $('.mainmenu-list').slideToggle('fast');
                  $('.mainmenu-list').addClass('mainmenu-colapsed');
                }else if ( $('.mainmenu-list').hasClass('mainmenu-colapsed') && $('.mainmenu-list').is(':visible') ) {
                  $('.mainmenu-list').slideToggle('fast');
                  $('.mainmenu-list').removeClass('mainmenu-colapsed');
                }

        });

        $( window ).resize(function() {
          var maxWindowWidth = 771;
          var windowWidth = window.screen.width;
          if (!$('.mainmenu-list').is(':visible') && windowWidth > maxWindowWidth )
          {
            $('.mainmenu-list').css({
              display: 'block'
            });
          }
        });


        $('.festivalpg-panelholder').click(function() {
          var element = $(this).find('.festivalpg-paneldescriptoin');
          $('.festivalpg-paneldescriptoin').not(element).stop( true, true ).slideUp('fast');

          $(this).find('.festivalpg-paneldescriptoin').stop( true, true ).slideToggle('fast');
        });


        $('.rollerPointer').click(function(event) {
          $('html, body').animate({
            scrollTop: $('.middle-scrollPoint').offset().top
          }, 1000);
        });


      });

  }

  /*load Builder and init start data*/
  constructor(props) {
    super(props);

    console.log("LOCATION:" + this.props.location.pathname);

    this.state = {
      path: this.props.location.pathname,
      data: null,
      top: null,
      middle: null,
      bottom: null
    };

    console.log('Initialization builder...');

    this.getData();
  }

  componentDidMount() {}

  /*receive a new url and reload data*/
  componentWillReceiveProps(nextProps) {

    this.state = {
      path: nextProps.location.pathname,
      data: null,
      top: null,
      middle: null,
      bottom: null
    };

    this.getData();

  }

  getData() {

    var parent = this;

    if (this.state.data === null) {
      console.log("Loading builder...");

      var apiUrl = 'http://api.example.com/' + window.btoa(this.state.path.slice(1, this.state.path.length));

      console.log("Location: " + apiUrl);
      console.log("End of link: " + this.state.path.slice(1, this.state.path.length));
      axios.get(apiUrl).then(res => {

        const data = res.data;
        this.setState({data: data});

        this.loadTopComponentData(this.state.data);
        this.loadMiddleComponentData(this.state.data);
        this.loadBottomComponentData(this.state.data);

      }).catch(function(error) {
        console.log(error);
        setTimeout(function() {
          parent.getData();
        }, 3000);
      });
    }

  }

  loadTopComponentData(config) {

    console.log("Start loading top component....");
    var apiUrl = config.top_component_endpoint;
    axios.get(apiUrl).then(res => {
      const data = res.data;
      //console.log(data);
      this.setState({top: data});

    });

  }

  loadMiddleComponentData(config) {

    console.log("Start loading middle component....");
    var apiUrl = config.middle_component_endpoint;
    axios.get(apiUrl).then(res => {
      const data = res.data;
      //console.log(data);
      this.setState({middle: data});

    });
  }

  loadBottomComponentData(config) {

    console.log("Start loading bottom component....");
    var apiUrl = config.bottom_component_endpoint;
    axios.get(apiUrl).then(res => {
      const data = res.data;
      //console.log(data);
      this.setState({bottom: data});

    });

  }

  render() {

    console.log(this.state);
    if (this.state.data !== null && this.state.top !== null && this.state.middle !== null && this.state.bottom !== null) {

      return (
        <div>
          <Header data={this.state.top} config={this.state.data}/>
          <Content data={this.state.middle} config={this.state.data} breadcrumbs={this.state.path}/>
          <Footer data={this.state.bottom} config={this.state.data}/>
          {this.loadJquery()}
        </div>
      );

    } else {
      return (
        <div className="spinner_background">
          <Spinner/>
        </div>
      );
    }

  }

}

export default withRouter(Builder);
