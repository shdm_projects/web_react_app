import React, { Component } from 'react';
import {Link} from 'react-router-dom';
import './Letters.css';

export default class Letters extends Component{

	constructor(props){
		super(props);

		var data = {...props.data};
		this.state = {
			data: data
		}
		//console.log("Letters -> ", this.state.data);
	}

	render(){

		var data = this.state.data;

		return (
			<div className="Letters">
				<div>
					<ul className="rightMenuhoder">
						{Object.keys(data).map(function(key) {

							var letter = data[key];

							return (
								<li className="rightMenuitems" key={key}>
									<Link className="rightMenuunit" to={'/'+letter.url+'/'+letter.name}>{letter.name}</Link>
								</li>
							);

						})}
					</ul>
				</div>
			</div>
		);

	}

}
