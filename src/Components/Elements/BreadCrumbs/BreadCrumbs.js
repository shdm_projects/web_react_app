import React, { Component } from 'react';
import {Link} from 'react-router-dom';
import './BreadCrumbs.css';

export default class BreadCrumbs extends Component{

	path = "";
	link = "";

	constructor(props){
		super(props);

		this.state = {
			breadcrumbs: this.props.breadcrumbs
		}

		var array = this.state.breadcrumbs.split("/");

		this.link = "";
		
		var arr = array[0].length === 0 ? array.shift() : null;

		arr = array[0].length === 2 ? array.shift() : null;

		for(var element in array){
			if(array[element].length){
				if(parseInt(element,10)+1 < array.length){
					this.link += "/" + array[element];
				}

				array[element] = array[element].charAt(0).toUpperCase() + array[element].slice(1);

				if(this.path.length){
					this.path += " / " +array[element];
				}else{
					this.path += array[element];
				}

			}
			
		}

	}

	render(){

		return (
			<div className="container">
				<div className="BreadCrumbs">
					<Link to={this.link} className="breadcrumbs_link">
		  				<span className="glyphicon glyphicon-arrow-left" aria-hidden="true">
		  				</span>
		  				<span className="breadcrumbs_back"><b>Wróć</b></span>
		  			</Link>
	  				<span className="breadcrumbs_path">{this.path}</span>
  				</div>
			</div>

		);

	}

}
