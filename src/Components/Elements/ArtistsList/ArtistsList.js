import React, { Component } from 'react';
import {Link} from 'react-router-dom';
import Letters from '../../Elements/Letters/Letters';
import './ArtistsList.css';

export default class Artists extends Component{

	constructor(props){
		super(props);
		//console.log("Artists");
		//console.log(props);
		this.state = {
			data: props.data.artists
		}
		//console.log(this.state.data);
	}

	render(){

		var artists = this.state.data;

		var arrayArtists = [];

		Object.keys(this.state.data).map(function(key,value) {

			arrayArtists.push(artists[key]);
			return 0;
		});

		return (
			<div>
			<div className="right col-xs-12 col-sm-12 ArtistsLettersMobile">
				<div className="artists-pageTitle">Artists</div>
				<Letters data={this.props.data}/>
			</div>
			<div className="left col-xs-12 col-sm-12 col-md-6 col-lg-6">
					<div className="artists-pageTitle artists-TitleDesktop">Artists</div>
						<div className="artists_list">

							{Object.keys(this.state.data).map(function(key,value) {

								    return (
								    	<ul className={key} key={value}>
								    		<div className="arists-alphabetwrapper" key={value}>
								    			<span className="arists-alphabetblock">{key}</span>
								    			<span className="line"></span>
								    			<span className="clearBoth"></span>
								    		</div>

								    		{arrayArtists[value].map(function(object, i){

								    			return (<li className="artistsAlphaberDescription" key={object.id}>
																	<Link className="artistsAlphaberDescriptionItem" to={"/"+object.url}>{object.name}</Link></li>);

								    		})}

								    	</ul>

								    );

							})}

					</div>
			</div>
			<div className="right col-xs-3 col-sm-3 col-md-3 col-lg-3 ArtistsLettersDesktop">
				<Letters data={this.props.data}/>
			</div>
		</div>
		);

	}

}
