import React, { Component } from 'react';
import {Link} from 'react-router-dom';
import './index.css';
import CalendarPicker from 'rc-calendar';
export default class View_type_7 extends Component{

	constructor(props){
		super(props);

		let data = {title: '', ...props.data};
		this.state = {
			data: data
		}
		console.log("View_type_7: ", this.state);
	}

	render(){
		var var1 = this.state.data.title?this.state.data.title:"";

		return (
			<div className="vt7 left col-xs-12 col-sm-9 col-md-9 col-lg-9">

				<div className="vt7 artists-pageTitle" dangerouslySetInnerHTML={{ __html: this.state.data.title }}></div>
				<div className="vt7 top_elements">
					<ul className="vt7 filters">
						
						{this.state.data.sort.map((object, i) => <li className="vt4 calendar-sortunit" key={i}>{object.link == '' ? object.name : <Link className="calendar-sortitem" to={object.link}>{object.name}</Link>}</li>)}
					
					</ul>
				</div>

					{
						this.state.data.multimedia.map(function(object, i){

							return (
								<div className="vt7 col-md-12 multimedia_items" key={i}>

									<div className="vt7 row">
										<div className="vt7 col-xs-2 col-sm-2 col-md-2 col-lg-2">
											<Link to={"/"+object.url}>
											<div className="vt7 calendarMzName">
												<div className="vt7 multimedia_item_date">{object.date} </div>
											</div>
											</Link>

										</div>
										<div className="vt7 col-xs-2 col-sm-3 col-md-3 col-lg-3">
											<Link to={"/"+object.url}><div className="vt7 multimedia_item_title"><b>{object.title} </b></div></Link>
											<div>{/* tutaj powinien być obiekt z osobami ktore brały udział w danym koncercie */}</div>

										</div>

										<div className="vt7 col-xs-7 col-sm-7 col-md-7 col-lg-7">

											{object.thumbs.map(function(object1, i){

												return(<img className="vt7 multimedia_thumbs" key={object1.id} src={object1.img} alt=""/>);

											})}

										</div>
									</div>

					        	</div>
					        );
						})
					}

			</div>
		);

	}

}
