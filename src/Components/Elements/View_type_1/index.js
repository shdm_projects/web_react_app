import React, { Component } from 'react';
import {Link} from 'react-router-dom';
// import RightMenu from '../../Elements/RightMenu/RightMenu';
//import HtmlToReactParser from 'html-to-react';
import './index.css';
//import CalendarPicker from 'rc-calendar';
export default class View_type_1 extends Component{

	constructor(props){
		super(props);
		//console.log("Artists");
		console.log("View_type_1: ", props);
		var data = Array.isArray(props.data) ? props.data[0] : props.data;
		this.state = {
			data: data,
			r_menu: data.right_menu?  data.right_menu:[],
			title: data.title ? data.title : '',
			tabs: data.tabs ? data.tabs : [],
			calendar: data.calendar ? data.calendar : [],
			content: data.content ? data.content : [],
			read_more: data.read_more ? data.read_more : [],
			list: data.list ? data.list : []
		}
		console.log(this.state);
	}

	createMarkup(item) {
	 return {__html: item}; 
	};

	render(){
		var _this = this;
		var var1 = this.state.data.title ? this.state.data.title : "";

		return (
			<div className="vt1">
				<div className="left col-xs-6 col-sm-6 col-md-6 col-lg-6">
					<div className="vt1-title row">
						<div className="vt1-title-name artists-pageTitle col-xs-6 col-sm-6 col-md-6 col-lg-6" dangerouslySetInnerHTML={{ __html: var1 }}></div>
						<div className="vt1-title-tabs col-xs-6 col-sm-6 col-md-6 col-lg-6">

							{this.state.tabs.map(function(object, i){

								if(object.active === 1){
									return(
										<div className="tab col-xs-6 col-sm-6 col-md-6 col-lg-6" key={i}><div className="tab-checked"><Link to={"/"+object.link}>{object.title}</Link></div></div>
									);
								}else{
									return(
										<div className="tab col-xs-6 col-sm-6 col-md-6 col-lg-6" key={i}><Link to={"/"+object.link}>{object.title}</Link></div>
									);
								}
								
							})}

						</div>
					</div>
					<div className="row">
					{this.state.data.list.map(function(object, i){

						

						if(object !== 'undefined' && object.hasOwnProperty('img') && !object.hasOwnProperty('text')){
							var color = object.title.backgroundColor;
							var img = object.img;
							var thumbnail = <div style={{flex: 1, backgroundColor: color, padding: 10, color: "white", position: 'absolute', left: 15, right: 15}}>{object.title.text}</div>
							return (

								<div className="col-xs-12 col-sm-12 col-md-12 col-lg-12" key={i}>
									<div>
									{thumbnail}
									<img src={img} className="list_image"/>
									</div>
						         </div>

							);

						}
						else if( object !== "undefined" && !object.hasOwnProperty('img')){

							var color = object.title ? object.title.backgroundColor : "";
							var button = null;
							var img = object.img;
							var thumbnail = <div className="middle-thumbnailholder" style={{flex: 1, backgroundColor: color, padding: 10, color: "white"}}>{object.title?object.title.text:""}</div>

							if(typeof(object.button) !== "undefined"){
		            			button = <div className="middle-addelem"><Link to={object.button.link} className="more_button"> </Link></div>;
							}
            
							return (
								<div className="col-xs-6 col-sm-6 col-md-6 col-lg-6 text-center" key={i}>
						          <div className="middle-postblock">
						          	{thumbnail}
						          	<div dangerouslySetInnerHTML={_this.createMarkup(object.text)}></div>
						            {button}
						          </div>
					        	</div>
							);

						}
						else if( object !== "undefined"){

							var color = object.title ? object.title.backgroundColor : "";
							var button = null;
							var img = object.img;
							var thumbnail = <div className="middle-thumbnailholder" style={{flex: 1, backgroundColor: color, color: "white", position: 'absolute', left: 25, right: 25, padding: 10}}>{object.title.text}</div>
							var img = <img src={img}/>;

							if(typeof(object.button) !== "undefined"){
		            			button = <div className="middle-addelem"><Link to={object.button.link} className="more_button"> </Link></div>;
							}
            
							return (
								<div className="col-xs-6 col-sm-6 col-md-6 col-lg-6 text-center" key={i}>
						          <div className="middle-postblock">
						          	<div>
						          	{thumbnail}
						          	{img}
						          	</div>
						          	<div dangerouslySetInnerHTML={_this.createMarkup(object.text)}></div>
						            {button}
						          </div>
					        	</div>
							);

						}

						
					})}
					</div>
				</div>
				<div className="right col-xs-3 col-sm-3 col-md-3 col-lg-3">
					<div className="vt1-calendar">
						<Link to={this.state.calendar.url}>
							<img className="vt1-calendar-img" src={this.state.calendar.img} />
							{this.state.calendar.text}
						</Link>
					</div>
					<div className="RightMenu">
						<ul className="right_menu_items">
							{this.state.r_menu.map(function(object, i){
								var var4 = object.url?object.url:"";
								var var5 = object.name?object.name:"";
								console.log("RightMenu", object);
								return(

										<li className="left-menuItem" key={i}><Link className="left-menuUnit" to={"/"+var4}>{var5}</Link></li>

									);
								})
							}

						</ul>
					</div>

				</div>
			</div>
		);

	}

}
