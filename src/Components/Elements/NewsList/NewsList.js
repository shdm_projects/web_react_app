import React, { Component } from 'react';
import './NewsList.css';


export default class NewsList extends Component{

	constructor(props){
		super(props);

		//console.log(props);
		this.state = {
			data: props.data
		}
		console.log('NewsList state ->', this.state.data);
	}

	render(){


		return (
			<div className="NewsList">
		      	<div className="row middle-scrollPoint">

						{this.state.data.map(function(object, i){
							//console.log(object);
							return(
								<div className="list-block" key={i}>
									<div className="title">{object.header}</div>
									{object.news_list.map(function(news, index){
										var var1 = news.category;
										var var2 = news.title;
										var var3 = news.description;
										var var4 = news.color;
										return (
											<div className="wrapped-item" key={index}>
									          <div className="middle-postblock">
									          	<div className="middle-thumbnailholder">
									            	<img  src={news.image} alt="" />
									              <div className="middlepost-thumbnail"></div>
									              <div className="load-element" style={{backgroundColor: var4}}></div>
									            </div>
									            <p className="middlepost-titldesc" dangerouslySetInnerHTML={{ __html: var1.toUpperCase() }}></p>
									            <p className="middlepost-titleitem" dangerouslySetInnerHTML={{ __html: var2 }}></p>
									            <p className="middlepost-description" dangerouslySetInnerHTML={{ __html: var3 }}></p>
									            <div className="middle-addelem">
									              <a href={"/"+news.url} className="more_button"> </a>
									            </div>
									          </div>
								        	</div>
										);

									})}
								</div>
							);
						})}

				</div>
			</div>
		);

	}

}
