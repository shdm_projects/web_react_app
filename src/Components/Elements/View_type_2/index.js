import React, { Component } from 'react';
import {Link} from 'react-router-dom';
// import RightMenu from '../../Elements/RightMenu/RightMenu';
//import HtmlToReactParser from 'html-to-react';
import './index.css';
//import CalendarPicker from 'rc-calendar';
export default class View_type_2 extends Component{

	constructor(props){
		super(props);
		console.log("View_type_2: ", props);
		var data = Array.isArray(props.data) ? props.data[0] : props.data;
		this.state = {
			data: data,
			r_menu: data.right_menu ? data.right_menu:[],
			title: data.title ? data.title : '',
			tabs: data.tabs ? props.data.tabs : [],
			calendar: data.calendar ? data.calendar : null,
			content: data.content ? data.content : [],
			read_more: data.read_more ? data.read_more : [],
			list: data.list ? data.list : []
		}
		//console.log(this.state);
	}

	createMarkup(item) {
	 return {__html: item}; 
	};


	render(){
		
		var _this = this;
		var var1 = this.state.data.title ? this.state.data.title : "";

		var calendar = this.state.calendar ? 
						<Link to={this.state.calendar.url}>
							<div className="vt2-calendar">
								<img className="vt2-calendar-img" src={this.state.calendar.img} />
								{this.state.calendar.text}
							</div>
						</Link>
						:null;
		
			return (
				<div className="vt2">
					<div className="left col-xs-6 col-sm-6 col-md-6 col-lg-6">
						<div className="vt2-title">
							<div className="vt2-title-name artists-pageTitle" dangerouslySetInnerHTML={{ __html: var1 }}></div>
							<div className="vt2-title-tabs">

								{this.state.tabs.map(function(object, i){

									if(object.active === 1){
										return(
											<div className="col-xs-6 col-sm-6 col-md-6 col-lg-6" key={i}><div className="tab-checked"><Link to={"/"+object.link}>{object.title}</Link></div></div>
										);
									}else{
										return(
											<div className="col-xs-6 col-sm-6 col-md-6 col-lg-6" key={i}><Link to={"/"+object.link}>{object.title}</Link></div>
										);
									}
									
								})}

							</div>
						</div>
						<div className="vt2-content">
							<div dangerouslySetInnerHTML={_this.createMarkup(this.state.content)}></div>
						</div>
						<div>
						{this.state.data.list.map(function(object, i){

							if(object.cols == 3){

								var color = 'rgba(0,156,178,0.5)';

								if(object !== 'undefined' && object.hasOwnProperty('img') && !object.hasOwnProperty('text')){

									var img = object.img;
									var thumbnail = <div style={{flex: 1, backgroundColor: color, padding: 10, color: "white", position: 'absolute', left: 15, right: 15}}>{object.title}</div>
									return (

										<div className="col-xs-12 col-sm-12 col-md-12 col-lg-12" key={i}>
											<div>
											{thumbnail}
											<img src={img} className="list_image"/>
											</div>
								         </div>

									);

								} else if( object !== "undefined"){

											
									var button = null;
									var img = object.img;
									var thumbnail = <div className="middle-thumbnailholder" style={{flex: 1, backgroundColor: color, padding: 10, color: "white"}}>{object.title}</div>
									var img = <div><img src={img}/></div>;

									if(typeof(object.button) !== "undefined"){
						    			button = <div className="middle-addelem"><Link to={object.button.link} className="more_button"> </Link></div>;
									}

									if(i == 0){
										return (
											<div className="vt2-col col-xs-4 col-sm-4 col-md-4 col-lg-4 text-center" key={i}>
									          <div className="middle-postblock">
									          	{img}
									          	<div dangerouslySetInnerHTML={_this.createMarkup(object.text)}></div>
									            {button}
									          </div>
								        	</div>
										);
									}else{
										return (
											<div className="vt2-col col-xs-4 col-sm-4 col-md-4 col-lg-4 text-center" key={i}>
									          <div className="middle-postblock">
									          	{img}
									          	<div dangerouslySetInnerHTML={_this.createMarkup(object.text)}></div>
									            {button}
									          </div>
								        	</div>
										);
									}
								}
							}else{
								var color = 'rgba(0,156,178,0.5)';

								if(object !== 'undefined' && object.hasOwnProperty('img') && !object.hasOwnProperty('text')){

									var img = object.img;
									var thumbnail = <div style={{flex: 1, backgroundColor: color, padding: 10, color: "white", position: 'absolute', left: 15, right: 15}}>{object.title}</div>
									return (

										<div className="col-xs-12 col-sm-12 col-md-12 col-lg-12" key={i}>
											<div>
											{thumbnail}
											<img src={img} className="list_image"/>
											</div>
								         </div>

									);

								} else if( object !== "undefined"){

											
									var button = null;
									var img = object.img;
									var thumbnail = <div className="middle-thumbnailholder" style={{flex: 1, backgroundColor: color, padding: 10, color: "white"}}>{object.title}</div>
									var img = <div><img src={img}/></div>;

									if(typeof(object.button) !== "undefined"){
						    			button = <div className="middle-addelem"><Link to={object.button.link} className="more_button"> </Link></div>;
									}

									if(i == 0){
										return (
											<div className="vt2-col col-xs-6 col-sm-6 col-md-6 col-lg-6 text-center" key={i}>
									          <div className="middle-postblock">
									          	{img}
									          	<div dangerouslySetInnerHTML={_this.createMarkup(object.text)}></div>
									            {button}
									          </div>
								        	</div>
										);
									}else{
										return (
											<div className="vt2-col col-xs-6 col-sm-6 col-md-6 col-lg-6 text-center" key={i}>
									          <div className="middle-postblock">
									          	{img}
									          	<div dangerouslySetInnerHTML={_this.createMarkup(object.text)}></div>
									            {button}
									          </div>
								        	</div>
										);
									}
								}
							}
							
						})}
						</div>
					</div>
					<div className="right col-xs-3 col-sm-3 col-md-3 col-lg-3">

						{calendar}

						<div className="RightMenu">
							<ul className="right_menu_items">
								{this.state.r_menu.map(function(object, i){
									var var4 = object.url?object.url:"";
									var var5 = object.name?object.name:"";
									//console.log("RightMenu", object);
									return(

											<li className="left-menuItem" key={i}><Link className="left-menuUnit" to={"/"+var4}>{var5}</Link></li>

										);
									})
								}

							</ul>
						</div>

					</div>
				</div>
			);
		
		

	}

}
