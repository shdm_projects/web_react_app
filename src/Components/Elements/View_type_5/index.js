import React, { Component } from 'react';
import {Link} from 'react-router-dom';
// import RightMenu from '../../Elements/RightMenu/RightMenu';
//import HtmlToReactParser from 'html-to-react';
import './index.css';
import RightMenu from '../RightMenu';
import CalendarButton from '../CalendarButton';
export default class View_type_5 extends Component{

	constructor(props){
		super(props);

		var data = {list:[], calendar:[], right_menu:[], tabs:[], ...props.data};
		this.state = {
			data: data,
			rightMenu: data.right_menu,
			tabs: data.tabs,
			calendar: data.calendar
		}
		console.log("View_type_5:", this.state);
	}

	createMarkup(item) {
	 return {__html: item}; 
	};

	render(){
		var title = this.state.data.title ? this.state.data.title : "";
		var _this = this;

		return (
			<div className="vt5">
				<div className="vt5 left col-xs-6 col-sm-6 col-md-6 col-lg-6">
					<div className="vt5 title">
						<div className="vt5 title-name artists-pageTitle" dangerouslySetInnerHTML={{ __html: title }}></div>
						<div className="vt5 title-tabs">

							{this.state.tabs.map(function(object, i){

								if(object.active == 1){
									return(
										<div className="vt5 col-xs-6 col-sm-6 col-md-6 col-lg-6" key={i}><div className="vt5 tab-checked"><Link to={"/"+object.link}>{object.title}</Link></div></div>
									);
								}else{
									return(
										<div className="vt5 col-xs-6 col-sm-6 col-md-6 col-lg-6" key={i}><Link to={"/"+object.link}>{object.title}</Link></div>
									);
								}
								
							})}

						</div>
					</div>
					<div>
					{this.state.data.list.map(function(object, i){

						var color = 'rgba(0,156,178,0.5)';

						if(object!=='undefined' && object.hasOwnProperty('img') && object.hasOwnProperty('title')){

							var img = object.img;
							var thumbnail = <div style={{flex: 1, backgroundColor: color, padding: 10, color: "white", position: 'absolute', left: 15, right: 15}}>{object.title}</div>
							return (

								<div className="vt5 content" key={i}>
									<div>
									{thumbnail}
									<img src={img} className="vt5 list_image"/>
									<div dangerouslySetInnerHTML={_this.createMarkup(object.text)}></div>
									</div>
						         </div>

							);

						}else{

							return (

								<div className="vt5 content" key={i}>
									<div>
									<img src={img} className="vt5 list_image"/>
									<div dangerouslySetInnerHTML={_this.createMarkup(object.text)}></div>
									</div>
						         </div>

							);

						}

						
					})}
					</div>
				</div>
				<div className="vt5 right col-xs-3 col-sm-3 col-md-3 col-lg-3">
					{this.state.calendar.length ? <CalendarButton data={this.state.calendar} /> : null}
					{this.state.rightMenu ? <RightMenu data={this.state.rightMenu} /> : null}
				</div>
			</div>
		);

	}

}
