import React, { Component } from 'react';
import {Link} from 'react-router-dom';
import './Multimedia.css';


export default class Multimedia extends Component{

	constructor(props){
		super(props);

		this.state = {
			data: props.data.multimedia
		}
		console.log(this.state.data);
	}

	render(){
		return (
			<div className="Multimedia">

				<div className="artists-pageTitle"><b>Multimedia</b></div>
				<div className="top_elements">

					<div className="filters">
						{/* <ul className="calendar-sortHolder">
							<span className="calendarExtendetsort">
								filtruj:
							</span>
							<li className="calendar-sortunit"><Link className="calendar-sortitem" to="">najnowsze</Link></li>
							<li className="calendar-sortunit"><Link className="calendar-sortitem" to="">zdjęcia</Link></li>
							<li className="calendar-sortunit"><Link className="calendar-sortitem" to="">audio</Link></li>
							<li className="calendar-sortunit"><Link className="calendar-sortitem" to="">wideo</Link></li>
							<li className="calendar-sortunit"><Link className="calendar-sortitem" to="">inne</Link></li>
						</ul> */}

					</div>
				</div>

					{
						this.state.data.map(function(object, i){

							return (
								<div className="col-md-12 multimedia_items" key={i}>

									<div className="row">
										<div className="col-xs-12 col-sm-2 col-md-2 col-lg-2">
											<Link to={"/"+object.url}>
											<div className="calendarMzName">
												<div className="multimedia_item_date">{object.date} </div>
											</div>
											</Link>

										</div>
										<div className="col-xs-12 col-sm-3 col-md-3 col-lg-3">
											<Link to={"/"+object.url}><div className="multimedia_item_title"><b>{object.title} </b></div></Link>
											<div>{/* tutaj powinien być obiekt z osobami ktore brały udział w danym koncercie */}</div>

										</div>

										<div className="col-xs-12 col-sm-7 col-md-7 col-lg-7">

											{object.thumbs.map(function(object1, i){

												return(<img className="multimedia_thumbs" key={object1.id} src={object1.img} alt=""/>);

											})}

										</div>
									</div>

					        	</div>
					        );
						})
					}

			</div>

		);

	}

}
