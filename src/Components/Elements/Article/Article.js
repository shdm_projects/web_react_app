import React, { Component } from 'react';
import './Article.css';
//import {Link} from 'react-router-dom';

export default class Article extends Component{

	constructor(props){
		super(props);

		this.state = {
			data: props.data.article
			//rightMenu: props.data.right_menu
		}
	}

	render(){
		return (
			<div className="Article">
				<div className="left">
					<div className="article_title artists-pageTitle" dangerouslySetInnerHTML={{ __html: this.state.data.title }}></div>
					<div className="article_img_block"><img src={this.state.data.cover_photo} className="article_img" alt=""/></div>
					<div className="article_lead" dangerouslySetInnerHTML={{ __html: this.state.data.lead }}></div>
					<div className="article_text" dangerouslySetInnerHTML={{ __html: this.state.data.text }}></div>
				</div>

			</div>
		);

	}

}
