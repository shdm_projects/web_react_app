import React, { Component } from 'react';
import {Link} from 'react-router-dom';
// import RightMenu from '../../Elements/RightMenu/RightMenu';
import HtmlToReactParser from 'html-to-react';
import './index.css';
import RightMenu from '../RightMenu';
import CalendarPicker from 'rc-calendar';
export default class View_type_4 extends Component{

	constructor(props){
		super(props);
		console.log("View_type_4: ", props);
		var data = {title: '', sort:[], events:[], right_menu:[], ...props.data};
		this.state = {
			data: data,
			rightMenu: data.right_menu

		}
		console.log(this.state);
	}

	render(){
		var events = this.state.data.events;
		var col = this.state.rightMenu.length ? 9 : 12;

			return (
				<div className="vt4 row">
					<div className="vt4 col-lg-12 artists-pageTitle"><b>{this.state.data.title}</b></div>
					<div className="vt4 col-lg-12 top_elements">

						<ul className="vt4 filters">
							
							{this.state.data.sort.map((object, i) => <li className="vt4 calendar-sortunit" key={i}>{object.link == '' ? object.name : <Link className="calendar-sortitem" to={object.link}>{object.name}</Link>}</li>)}
							
						</ul>

					</div>

						<div className={"vt4 col-lg-"+col}>

						{

							Object.keys(events).map(function(key) {

								var obj = events[key];

								return(

									<div className="vt4" key={key}>

										<div className="vt4 col-lg-12 calendar-timeset">
											<div className="vt4 calendarTimesetunit">
												{key}
											</div>
											<div className="vt4 calendarWeekDay">
											</div>
										</div>

										{
											Object.keys(obj).map(function(key1) {

												var item = obj[key1];

											    return(

														<div className="vt4 row" key={key1}>
															<div className="vt4 col-xs-4 col-sm-4 col-md-4 col-lg-4">
																<div className="vt4 calendarMzName">
																	<div className="vt4 calendarMzNameItem calendarMzTimeItem">{item.time}</div>
																	<div className="vt4 calendarMzNameItem">{item.place}</div>
																</div>

															</div>
															<div className="vt4 col-xs-5 col-sm-5 col-md-5 col-lg-5">
																<div className="vt4 center">
																	<div className="vt4 calendarContTitle">{item.type}</div>

																	<div className="vt4 calendarParticipantWrapper">
																		{item.artists.map(function(object, i){

																			return(<div className="vt4 participantBlock" key={i}><span className="vt4 calendarNameParticipant" ><Link to={object.link}>{ object.name }</Link></span><span> {object.instrument} </span></div>);

																		})}
																	</div>

																	<div className="vt4 calendarProgrammBox">PROGRAM</div>
																	<div className="vt4 calendarParticipantExtendet" dangerouslySetInnerHTML={{ __html: item.lineup }}></div>
																	<div className="vt4 middle-addelem">
														              <a href={"/"+item.url} className="vt4 more_button"> </a>
														            </div>
																</div>
															</div>

															<div className="vt4 col-xs-3 col-sm-3 col-md-3 col-lg-3">
																<ul className="vt4 calendarListRightMenu">
																	
																		{item.links.map((object, i) => <li className="vt4 calendarListItemRightMenu" key={i}><Link to={object.link}>{ object.name }</Link></li>)}
																
																</ul>
															</div>
														</div>
										        );
								        	})
										}
									</div>
								);
							})

						}
						</div>
						<div className="vt5 right col-xs-3 col-sm-3 col-md-3 col-lg-3">
							{this.state.rightMenu.length ? <RightMenu data={this.state.rightMenu} /> : null}
						</div>
				</div>
			);
		}

}
