import React, { Component } from 'react';
import './MultimediaItem.css';
import Video from '../Videos';
import Images from '../Images';

export default class MultimediaItem extends Component{

	constructor(props){
		super(props);
		console.log(props.data);
		/*this.state = {
			data: props.data.multimedia
		}*/
		this.state = {
	      open: false,
	      showPlayButton: true,
	      showGalleryPlayButton: false,
	      showFullscreenButton: true,
	      showGalleryFullscreenButton: false,
	      currentImage: 0,
	      player: [],
	      data: props.data.multimedia
	    };

		console.log("LIST");
		console.log(this.state.data);
	}



	render(){

	var images = null;
	var videos = null;

    //console.log(this.state.currentImage);


    if(this.state.data.hasOwnProperty('images')){
    	images = <Images data={this.state.data} />
    }

    if(this.state.data.hasOwnProperty('videos')){
    	videos = <Video data={this.state.data} />
    }

		return (
			<div className="MultimediaItem">

				<div className="artists-pageTitle">{this.state.data.date} / {this.state.data.title}</div>
				<div className="top_elements">
					<div className="line"></div>
				</div>

				{images}

				{videos}

			</div>

		);

	}

}
