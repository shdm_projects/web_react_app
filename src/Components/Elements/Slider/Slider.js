import React, { Component } from 'react';
import img_l from '../../../assets/images/arrow_right.png';
import img_r from '../../../assets/images/arrow_left.png';
import img_scroll from '../../../assets/images/button_element.png';

export default class Slider extends Component{

	constructor(props){
		super(props);
		this.state = {
			slides: JSON.parse(this.props.data.slides) ? JSON.parse(this.props.data.slides) : []
		}
	}

	render(){

			var carouselControl = (this.state.slides.length > 1) ? 
			<div><a className="left carousel-control" href="#carousel-example-generic" data-slide="prev"><img src={img_l} alt="nifc"/></a>
			<a className="right carousel-control" href="#carousel-example-generic" data-slide="next"><img src={img_r} alt="nifc"/></a></div>:'';

			return (

			<div className="karuzela">
				<center className="roller"><img src={img_scroll} className="rollerPointer" alt="nifc"/></center>
				<div id="carousel-example-generic" className="carousel slide" data-ride="carousel">
					<div className="carousel-inner">

						{this.state.slides.map(function(object, i){

							var img = object.img;

							if(i === 0){
								return(

									<div className="item active" key={i}>

										<div className="slaider-bgimage" style={{backgroundImage: "url("+ img +")"}}></div>
										<div className="carousel-caption">
											<div className="container">
												<div className="row">
									 				<div className="col-sm-3 col-md-2 col-lg-2"></div>
													<div className="col-xs-12 col-sm-4 col-md-6 col-lg-6">
														<div className="headerinfo-block">
															<p className="headerinfo-title">
																{object.title}
															</p>
															<p className="headerinfo-content">
																{object.description}
															</p>
															<p className="headerinfo-timeref">
																{object.date}
															</p>
														</div>
													</div>
													<div className="col-sm-3 col-md-3 col-lg-3"></div>
												</div>
											</div>
										</div>
									</div>

								);
							}
							return(

									<div className="item" key={i}>

										<div className="slaider-bgimage" style={{backgroundImage: "url("+ img +")"}}></div>
										<div className="carousel-caption">
											<div className="container">
												<div className="row">
													<div className="col-sm-3 col-md-2 col-lg-2"></div>
													<div className="col-xs-12 col-sm-4 col-md-6 col-lg-6">
														<div className="headerinfo-block">
															<p className="headerinfo-title">
																{object.title}
															</p>
															<p className="headerinfo-content">
																{object.description}
															</p>
															<p className="headerinfo-timeref">
																{object.date}
															</p>
														</div>
													</div>
													<div className="col-sm-3 col-md-3 col-lg-3"></div>
												</div>
											</div>
										</div>
									</div>

								);

						})}

						{carouselControl}
						

				</div>
			</div>
		</div>


				);

	}

}
