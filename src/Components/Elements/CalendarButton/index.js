import React, { Component } from 'react';
import {Link} from 'react-router-dom';
import './index.css';

export default class CalendarButton extends Component{

	constructor(props){
		super(props);
		
		
		var data = {url:'', img:'', text:'', ...props.data};

		this.state = {
			data: data
		}
		console.log("CalendarButton: ", this.state);
	}

	render(){

		return (
			<div className="calendar">
				<Link to={this.state.data.url}>
					<div className="calendar-content">
						<img className="calendar-img" src={this.state.data.img} />
						<span className="calendar-text">{this.state.data.text} </span>
					</div>
				</Link>
			</div>
		);

	}

}
