import React, { Component } from 'react';
import {Link} from 'react-router-dom';
// import RightMenu from '../../Elements/RightMenu/RightMenu';
import HtmlToReactParser from 'html-to-react';
import './index.css';
import CalendarPicker from 'rc-calendar';
export default class Education1 extends Component{

	constructor(props){
		super(props);
		//console.log("Artists");
		console.log("Education: ", props);
		this.state = {
			data: props.data[0],
			r_menu: props.data[0].right_menu?props.data[0].right_menu:[],
		}
		console.log(this.state);
	}

	render(){
		var var1 = this.state.data.title?this.state.data.title:"";

		return (
			<div className="Education">
				<div className="left col-xs-6 col-sm-6 col-md-6 col-lg-6">
					<div className="education-title">
						<div className="education-title-name artists-pageTitle" dangerouslySetInnerHTML={{ __html: var1 }}></div>
						<div className="education-title-tabs">

							{this.state.data.tabs.map(function(object, i){

								if(object.active == 1){
									return(
										<div className="col-xs-6 col-sm-6 col-md-6 col-lg-6" key={i}><div className="tab-checked"><Link to={"/"+object.link}>{object.title}</Link></div></div>
									);
								}else{
									return(
										<div className="col-xs-6 col-sm-6 col-md-6 col-lg-6" key={i}><Link to={"/"+object.link}>{object.title}</Link></div>
									);
								}
								
							})}

						</div>
					</div>
					<div>
					{this.state.data.list.map(function(object, i){

						var color = 'rgba(0,156,178,0.5)';

						if(object!=='undefined' && object.hasOwnProperty('img') && !object.hasOwnProperty('text')){

							var img = object.img;
							var thumbnail = <div style={{flex: 1, backgroundColor: color, padding: 10, color: "white", position: 'absolute', left: 15, right: 15}}>{object.title}</div>
							return (

								<div className="col-xs-12 col-sm-12 col-md-12 col-lg-12" key={i}>
									<div>
									{thumbnail}
									<img src={img} className="list_image"/>
									</div>
						         </div>

							);

						} else if( object !== "undefined"){

							var img = null;
							var button = null;
							
							var thumbnail = <div className="middle-thumbnailholder" style={{flex: 1, backgroundColor: color, padding: 10, color: "white"}}>{object.title}</div>
							if(object.hasOwnProperty('img') && object.img.length != 0){
								img = <div><img src={object.img}/></div>;	
							}
							
							var html = HtmlToReactParser.Parser();
							var reactElements = html.parse(object.text);

							if(typeof(object.button) !== "undefined"){
		            			button = <div className="middle-addelem"><Link to={object.button.link} className="more_button"> </Link></div>;
							}
            
							return (
								<div className="col-xs-6 col-sm-6 col-md-6 col-lg-6 text-center" key={i}>
						          <div className="middle-postblock">
						          	{img}
						          	{reactElements}
						            {button}
						          </div>
					        	</div>
							);

						}

						
					})}
					</div>
				</div>
				<div className="right col-xs-3 col-sm-3 col-md-3 col-lg-3">
					<CalendarPicker />
					<div className="RightMenu">
						<ul className="right_menu_items">
							{this.state.r_menu.map(function(object, i){
								var var4 = object.url?object.url:"";
								var var5 = object.name?object.name:"";
								console.log("RightMenu", object);
								return(

										<li className="left-menuItem" key={i}><Link className="left-menuUnit" to={"/"+var4}>{var5}</Link></li>

									);
								})
							}

						</ul>
					</div>

				</div>
			</div>
		);

	}

}
