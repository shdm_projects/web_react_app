import React, { Component } from 'react';
import {Link} from 'react-router-dom';
// import RightMenu from '../../Elements/RightMenu/RightMenu';
import HtmlToReactParser from 'html-to-react';
import './index.css';
import CalendarPicker from 'rc-calendar';
export default class Education1 extends Component{

	constructor(props){
		super(props);
		console.log("Education: ", props);
		this.state = {
			data: props.data[0],
			r_menu: props.data[0].right_menu?props.data[0].right_menu:[],
		}
		//console.log(this.state);
	}

	createMarkup(text) { return {__html: text}; };

	render(){
		var var1 = this.state.data.title?this.state.data.title:"";
		var _this= this;
		return (
			<div className="Education">
				<div className="left col-xs-6 col-sm-6 col-md-6 col-lg-6">
					<div className="title">
						<div className="article_title artists-pageTitle" dangerouslySetInnerHTML={{ __html: var1 }}></div>
					</div>
					<div>
					{this.state.data.list.map(function(object, i){
						var button = null;

						var color = "rgba(0,156,178,1)";

						switch(i){
							case 0:
							color = "rgba(0,156,178,1)";
							break;
							case 1:
							color = "rgba(0,170,107,1)";
							break;
							default:
							break;
						}

						var thumbnail = <div className="middle-thumbnailholder" style={{flex: 1, backgroundColor: color, padding: 10, color: 'white'}}>{object.title}</div>;

						if(typeof(object.button) !== "undefined"){
	            			button = <div className="middle-addelem">
				              			<a href={"/"+object.button.link} className="more_button"> </a>
				            		</div>;
						}
            
						return (
									<div className="List col-xs-6 col-sm-6 col-md-6 col-lg-6 text-center" key={i}>
							          <div className="middle-postblock">
							          	{thumbnail}
							          	<div dangerouslySetInnerHTML={_this.createMarkup(object.text)} />
							            {button}
							          </div>
						        	</div>
								);
					})}
					</div>
				</div>
				<div className="right col-xs-3 col-sm-3 col-md-3 col-lg-3">
					<CalendarPicker />
					<div className="RightMenu">
						<ul className="right_menu_items">
							{this.state.r_menu.map(function(object, i){
								var var4 = object.url?object.url:"";
								var var5 = object.name?object.name:"";
								console.log("RightMenu", object);
								return(

										<li className="left-menuItem" key={i}><Link className="left-menuUnit" to={"/"+var4}>{var5}</Link></li>

									);
								})
							}

						</ul>
					</div>

				</div>
			</div>
		);

	}

}
