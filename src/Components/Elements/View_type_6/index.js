import React, { Component } from 'react';
import {Link} from 'react-router-dom';
import './index.css';
import RightMenu from '../RightMenu';
import CalendarButton from '../CalendarButton';
import Letters from '../../Elements/Letters/Letters';

export default class View_type_6 extends Component{

	constructor(props){
		super(props);

		var data = {artists:[], letters:[], title: '', ...props.data};
		this.state = {
			data: data
		}
		console.log("View_type_6:", this.state);
	}

	createMarkup(item) {
	 return {__html: item}; 
	};

	render(){

		var artists = this.state.data.artists;

		var arrayArtists = [];

		Object.keys(this.state.data.artists).map(function(key,value) {

			arrayArtists.push(artists[key]);
			return 0;
		});



		return (
			<div>	
			<div className="vt6 right col-xs-12 col-sm-12 ArtistsLettersMobile">
				<div className="vt6 artists-pageTitle">{this.state.data.title}</div>
				{this.state.data.letters.length ? <Letters data={this.state.data.letters}/> : null}
			</div>
			<div className="vt6 left col-xs-12 col-sm-12 col-md-6 col-lg-6">
					<div className="vt6 artists-pageTitle artists-TitleDesktop">{this.state.data.title}</div>
						<div className="vt6 artists_list">

							{Object.keys(artists).map(function(key,value) {
									
								    return (
								    	<ul className={key} key={value}>
								    		<div className="vt6 arists-alphabetwrapper" key={value}>
								    			<span className="vt6 arists-alphabetblock">{key}</span>
								    			<span className="vt6 line"></span>
								    			<span className="vt6 clearBoth"></span>
								    		</div>

								    		{arrayArtists[value].map(function(object, i){

								    			return (
								    				<li className="vt6 artistsAlphaberDescription" key={object.id}>
														<Link className="vt6 artistsAlphaberDescriptionItem" to={"/"+object.url}>{object.name}</Link>
													</li>
												);

								    		})}

								    	</ul>

								    );

							})}

					</div>
			</div>
			<div className="vt6 right col-xs-3 col-sm-3 col-md-3 col-lg-3 ArtistsLettersDesktop">
				{this.state.data.letters.length ? <Letters data={this.state.data.letters}/> : null}
			</div>
		</div>
		);

	}

}
