import React, { Component } from 'react';
import {Link} from 'react-router-dom';
import img_logo from '../../../assets/images/Logo.svg';
// import img_element from '../../../assets/images/element_menu.png';
// import img_element_2 from '../../../assets/images/Shop bag.svg';
// import img_element_3 from '../../../assets/images/Search loupe.svg';
// import img_element_4 from '../../../assets/images/White arrow down.svg';

export default class Menu extends Component{

	constructor(props){
		super(props);

		this.state = {
			data: JSON.parse(props.data.main_menu),
			extend_nav: JSON.parse(props.data.extend_nav)
		}

	}

	render(){


		return(


			<div className="container Menu">
				<div className="row">

						<div className="col-xs-4 col-sm-3 col-md-2 col-lg-2">
							<img src={img_logo} alt="nifc" className="img-responsive img-logocust"/>
						</div>
						<div className="col-xs-4 col-sm-6 col-md-7 col-lg-7 extraset-item">

								<ul className="nav navbar-nav mainmenu-list-colaps mainmenu-list">

									{this.state.data.map(function(object, i){
										if(object.hasOwnProperty('first')){
											return <li className="offset-elem navel-item navfirst-elm" key={i}><Link to={object.url} className="offset-unit navel-unit">{object.name}</Link></li>
										}else{
											return <li className="offset-elem navel-item" key={i}><Link to={object.url} className="offset-unit navel-unit">{object.name}</Link></li>
										}
									})}

								</ul>

						</div>
						<div className="col-xs-3 col-sm-3 col-md-3 col-lg-3">
								<div className="header-extentnav-wrapper" id="">
									<nav className="navbar navbar-default">
									  <div className="container-fluid">

										<div className="navbar-header">
										  <button type="button" className="navbar-toggle collapsed" data-toggle="collapse" data-target="#bs-example-navbar-collapse-1" aria-expanded="false">
											<span className="sr-only">Toggle navigation</span>
											<span className="icon-bar"></span>
											<span className="icon-bar"></span>
											<span className="icon-bar"></span>
										  </button>

										</div>


										<div className="collapse navbar-collapse" id="bs-example-navbar-collapse-1">
										  <ul className="nav navbar-nav header-extentnav-holder">
										  {this.state.extend_nav.map(function(object, i){

										  	return (
											  	<li className="header-extendetnav-item" key={i}>
											  		<Link to={object.url} className="navel-unit">
											  			<img src={object.img} className="extendet-navigation" alt="nifc"/>
											  		</Link>
											  	</li>
										  	);

										  })}
										  	
										  </ul>
										</div>
									  </div>
									</nav>
								</div>
						</div>
					</div>

				</div>




			);



	}

}
