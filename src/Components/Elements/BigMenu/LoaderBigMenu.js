import BigMenu from '../../Elements/BigMenu/BigMenu';
import React, { Component } from 'react';


export default class LoaderBigMenu extends Component{


	render(){

		return(
			<div className="container">
				<BigMenu data={this.props.data} />
			</div>
		)
	}
}