import React, { Component } from 'react';
import {Link} from 'react-router-dom';
import './BigMenu.css';

export default class BigMenu extends Component {

	constructor(props){
		super(props);
		//console.log("big");
		//console.log(props);
		this.state = {
			data: props.data.boxes
		}
		//console.log(this.state.data);
	}

	render() { 
		return (
			<div className="BigMenu">
				<div className="row">
					<div className="gb-parentpanel-hoder">
						<div className="concurs-panels-holder panels-holder-desctopset">
							<div className="festivalpg-panelholder-mask"></div>
								{this.state.data.map(function(object, i){
									return (
											<div className="festivalpg-panelholder" key={object.id} style={{borderColor: object.color}}>
													{ object.sub_menu ? (
														<div className="festivalpg-panelbox">
															<span className="festival-hover-mask"></span>
															<div className="fest-hover-desc">
																<p className="festivalpg-paneltitle">
																	{object.name}
																</p>
																<div className="festivalpg-paneldescriptoin">
																	<ul className="panel-menu-list">
																		{ object.sub_menu ? (
																				object.sub_menu.map(function(object1, a){
																					return (<li key={a}><Link to={object1.url}>{object1.name}</Link></li>);
																				})
																			):('')
																		}
																		</ul>
																	</div>
																</div>
															</div>
														):(
															<Link to={object.url}>
																<div className="festivalpg-panelbox">
																	<span className="festival-hover-mask"></span>
																	<div className="fest-hover-desc">
																		<p className="festivalpg-paneltitle">
																			{object.name}
																		</p>
																		<div className="festivalpg-paneldescriptoin">
																			<ul className="panel-menu-list">
																				{ object.sub_menu ? (
												 										object.sub_menu.map(function(object1, a){
																							return (<li key={a}><Link to={object1.url}>{object1.name}</Link></li>);
																						})
																					):('')
																				}
																			</ul>
																		</div>
																	</div>
																</div>
															</Link>
														)
													}
											</div>
											);
								})}
						</div>
					</div>
				</div>
			</div>
		);

	}

}
