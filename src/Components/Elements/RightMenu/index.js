import React, { Component } from 'react';
import {Link} from 'react-router-dom';
import './index.css';

export default class RightMenu extends Component{

	constructor(props){
		super(props);

		this.state = {
			rightMenu: props.data
		}

	}

  render(){
	  return (

				<div className="RightMenu disctopMenuright">
					<ul className="RightMenu list">
						{this.state.rightMenu.map(function(object, i){

						return (<li className="RightMenu item" key={object.id}><Link to={"/"+object.url}>{object.name}</Link></li>);
						})}
					</ul>
				</div>

    );
  }
}
