import React, { Component } from 'react';
import {Link} from 'react-router-dom';
import './RightMenu.css';

export default class RightMenu extends Component{

	constructor(props){
		super(props);

		this.state = {
			rightMenu: props.data
		}

	}

  render(){
	  return (
          	<div className="right col-xs-2 col-sm-2 col-md-3 col-lg-3">

				<div className="disctopMenuright">
					<ul>
						{this.state.rightMenu.map(function(object, i){

						return (<li className="about-grightMenu" key={object.id}><Link to={"/"+object.url}>{object.name}</Link></li>);
						})}
					</ul>
				</div>
			</div>
    );
  }
}
