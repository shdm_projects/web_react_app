import React, { Component } from 'react';
import {Link} from 'react-router-dom';
// import RightMenu from '../../Elements/RightMenu/RightMenu';
import HtmlToReactParser from 'html-to-react';
import './index.css';
import CalendarPicker from 'rc-calendar';
export default class View_type_9 extends Component{

	constructor(props){
		super(props);
		var data = {list:[], ...props.data};
		this.state = {
			data: data
		}
		console.log("View_type_9: ", this.state);
	}

	render(){
		var object = this.state.data;

		return (
			<div className="vt9">
				<div className="vt9 col-xs-9 col-sm-9 col-md-9 col-lg-9">
					<div className="row">
						<div className="vt9 title">{object.header}</div>
						<div className="vt9 ">
							{object.list.map(function(news, index){
								var category = news.category;
								var title = news.title;
								var description = news.description;
								var color = news.color;
								var image = news.image;
								var url = news.url;
								return (
									<div className="vt9 wrapped-item col-xs-3 col-sm-3 col-md-3 col-lg-3" key={index}>
										<div className="vt9 middle-postblock">
											<div className="vt9 middle-thumbnailholder">
												<img  src={image} alt="" />
												<div className="vt9 middlepost-thumbnail"></div>
											</div>
											<p className="vt9 middlepost-titldesc" dangerouslySetInnerHTML={{ __html: category.toUpperCase() }}></p>
											<p className="vt9 middlepost-titleitem" dangerouslySetInnerHTML={{ __html: title }}></p>
											<p className="vt9 middlepost-description" dangerouslySetInnerHTML={{ __html: description }}></p>
											<div className="vt9 middle-addelem">
												<Link className="vt9 more_button" to={"/"+url}></Link>
											</div>
										</div>
									</div>

								);

								/*return (
										<div className="vt9">
											<div className="vt9 wrapped-item" key={index}>
												<div className="vt9 middle-postblock">
													<div className="vt9 middle-thumbnailholder">
														<img  src={image} alt="" />
														<div className="vt9 middlepost-thumbnail"></div>
														<div className="vt9 load-element" style={{backgroundColor: color}}></div>
													</div>
													<p className="vt9 middlepost-titldesc" dangerouslySetInnerHTML={{ __html: category.toUpperCase() }}></p>
													<p className="vt9 middlepost-titleitem" dangerouslySetInnerHTML={{ __html: title }}></p>
													<p className="vt9 middlepost-description" dangerouslySetInnerHTML={{ __html: description }}></p>
													<div className="vt9 middle-addelem">
														<Link className="vt9 more_button" to={"/"+url}></Link>
													</div>
												</div>
											</div>
										</div>
								);*/

							})}
							</div>
							</div>
						</div>
			</div>
		);

	}

}
