  import React, { Component } from 'react';
  import {Link} from 'react-router-dom';
  //import DatePicker from 'react-datepicker';
  import 'react-datepicker/dist/react-datepicker.css';
  // import calendar from '../../../assets/images/Calendar - circle.svg';
  import './Calendar.css';

export default class Calendar extends Component{

	constructor(props){
		super(props);
		console.log("Calendar");
		console.log(props);
		this.state = {
			data: props.data.events
		}
		console.log(this.state.data);

	}

	render(){

		var datas = this.state.data;

		return (
			<div className="Calendar">
				<div className="artists-pageTitle"><b>Kalendarium</b></div>
				<div className="top_elements">

					<div className="filters">
						{/*<ul className="calendar-sortHolder">
							<span className="calendarExtendetsort">
								sortuj wg.:
							</span>
							<li className="calendar-sortunit"><Link className="calendar-sortitem" to="/festiwal/kalendarium/data">data</Link></li>
							<li className="calendar-sortunit"><Link className="calendar-sortitem" to="/festiwal/kalendarium/artysci">artysci</Link></li>
							<li className="calendar-sortunit"><Link className="calendar-sortitem" to="/festiwal/kalendarium/miejsce">miejsce</Link></li>
							<li className="calendar-sortunit"><Link className="calendar-sortitem" to="/festiwal/kalendarium/koncert-kameralny">koncert kameralny</Link></li>
							<li className="calendar-sortunit"><Link className="calendar-sortitem" to="/festiwal/kalendarium/koncert-symfoniczny">koncert symfoniczny</Link></li>
							<li className="calendar-sortunit"><Link className="calendar-sortitem" to="/festiwal/kalendarium/dla-dzieci">dla dzieci</Link></li>
							<li className="calendar-sortunit"><Link className="calendar-sortitem" to="/festiwal/kalendarium/inne">inne</Link></li>
							<li className="calendar-sortunit"><Link className="calendar-sortitem" to="">
							<img src={calendar} className="extendet-navigation1" alt="nifc"/></Link></li>
						</ul>*/}

					</div>

				</div>

					{

						Object.keys(datas).map(function(key) {

							var obj = datas[key];

							return(

								<div className="col-md-12" key={key}>

									<div className="calendar-timeset">
										<div className="calendarTimesetunit">
											{key}
										</div>
										<div className="calendarWeekDay">

										</div>
									</div>

									{
										Object.keys(obj).map(function(key1) {

											var object = obj[key1];

										    return(

													<div className="row" key={key1}>
														<div className="col-xs-12 col-sm-4 col-md-4 col-lg-4">
															<div className="calendarMzName">
																<div className="calendarMzNameItem calendarMzTimeItem">{object.time}</div>
																<div className="calendarMzNameItem">{object.place}</div>
															</div>

														</div>
														<div className="col-xs-12 col-sm-5 col-md-5 col-lg-5">
															<div className="center">
																<div className="calendarContTitle">{object.type}</div>

																<div className="calendarParticipantWrapper">
																	{object.artists.map(function(object1, i){

																		return(<div className="participantBlock" key={i}><span className="calendarNameParticipant" ><Link to={object1.link}>{ object1.name }</Link></span><span> {object1.instrument} </span></div>);

																	})}
																</div>

																<div className="calendarProgrammBox">PROGRAM</div>
																<div className="calendarParticipantExtendet" dangerouslySetInnerHTML={{ __html: object.lineup }}></div>
																<div className="calendartakemorButtonz">
																	<a href={object.url} className="more_buttonz"> </a>
                                  <hr />
                                  {/* dodany hr oraz Z aby ukryć (+) pod koncertem */}
																</div>
															</div>
														</div>

														<div className="col-xs-12 col-sm-3 col-md-3 col-lg-3">
															<div className="calendarsubmenu">
																<div className="calendarOrderbox">test</div>
															</div>
														</div>
													</div>
									        );
							        	})
									}
								</div>
							);
						})

					}

			</div>
		);

	}

}
