import React, { Component } from 'react';
import {Link} from 'react-router-dom'; 
import './LeftMenu.css';


export default class LeftMenu extends Component{

	constructor(props){
		super(props);
		console.log(props);
		this.state = {
			data: props.data.hasOwnProperty('left_menu') ? props.data.left_menu : []
		}
	}

	render(){


		if(this.state.data.length)

			return (
				<div className="LeftMenu">
					<ul className="left_menu_items">
						{this.state.data.map(function(object, i){
							if(object.hasOwnProperty('submenu') && object.submenu === true){
								return(

									<li className="left-menuItem" key={i}><Link className="left-subMenuUnit" to={"/"+object.url}>{object.name}</Link></li>

								);
							}else{
								return(

									<li className="left-menuItem" key={i}><Link className="left-menuUnit" to={"/"+object.url}>{object.name}</Link></li>

								);
							}})
						}

					</ul>
				</div>
			)
		else
			return null;

	}

}
