import React, { Component } from 'react';
import {Link} from 'react-router-dom'; 
import './Artists.css';

export default class Artists extends Component{

	constructor(props){
		super(props);
		//console.log("Artists");
		//console.log(props);
		this.state = {
			data: props.data.artists
		}
		//console.log(this.state.data);
	}

	render(){

		var artists = this.state.data;

		var arrayArtists = [];

		Object.keys(this.state.data).map(function(key,value) {
			
			arrayArtists.push(artists[key]);
			return 0;
		});

		return (
			<div className="Artists">
				<div className=""><b>Artists</b></div>
			
					<div className="artists_list">

						{Object.keys(this.state.data).map(function(key,value) {

							    return (
							    	<ul className={key} key={value}>

							    		<div key={value}>
							    			<span>{key}</span>
							    			<span className="line"></span>
							    		</div>

							    		{arrayArtists[value].map(function(object, i){

							    			return (<li key={object.id}><Link to={"/"+object.url}>{object.name}</Link></li>);


							    		})}

							    	</ul>

							    );

						})}
				
					</div>
				
			</div>
		);

	}

}
