import React, { Component } from 'react';
import {Link} from 'react-router-dom';
import './index.css';
import Videos from '../Videos';
import Images from '../Images';

export default class View_type_8 extends Component{

	constructor(props){
		super(props);
		//console.log("Artists");
		console.log("View_type_8: ", props);
		let data = {multimedia: [], ...props.data};
		this.state = {
	      open: false,
	      showPlayButton: true,
	      showGalleryPlayButton: false,
	      showFullscreenButton: true,
	      showGalleryFullscreenButton: false,
	      currentImage: 0,
	      player: [],
	      data: data
	    };
		console.log(this.state);
	}

	render(){
		let multimedia = this.state.data.multimedia || [];
		let pageTitle = multimedia.date + " / " + multimedia.title;
		let images = multimedia.images || null;
		let videos = multimedia.videos || null;

	    console.log(this.state.currentImage);


	    if(images){
	    	images = <Images data={images} />
	    }

	    if(videos){
	    	videos = <Videos data={videos} />
	    }

			return (
				<div className="vt8 container-fluid">
					<div className="vt8 row">
						<div className="vt8 artists-pageTitle col-xs-12 col-sm-12 col-md-12 col-lg-12">
							<p className="h1">{pageTitle}</p>
						</div>
						<div className="vt8 col-xs-12 col-sm-12 col-md-12 col-lg-12">
							<div className="vt8 line"></div>
						</div>
						<div className="vt8 col-xs-12 col-sm-12 col-md-12 col-lg-12">
							{images}
							{videos}
						</div>
					</div>
				</div>

			);

	}

}
