import React, { Component } from 'react';
import './List2.css';
//import {Link} from 'react-router-dom';

export default class List2 extends Component{

	constructor(props){
		super(props);
		console.log(props.data);
		this.state = {
			data: props.data.catalog,

		}
		console.log("LIST2");
		console.log(props.data.catalog);
	}

	render(){
		return (
<div>

					{
						this.state.data.map(function(object, i){
							return (
								<div className="List2 col-xs-6 col-centered col-sm-6 col-md-6 col-lg-6 text-center" key={object.id}>
										<div className="title">{object.main_title}</div>
						          <div className="middle-postblock">
						          	<div className="middle-ramka">
						            	<div className="kolorek" style={{backgroundColor: object.box_color}}>{object.box_title}</div>
						              <div className="middlepost-thumbnail"></div>

						            </div>
						            <p className="middlepost-titleitem" dangerouslySetInnerHTML={{ __html: object.title }}></p>
						            <p className="middlepost-description" dangerouslySetInnerHTML={{ __html: object.description }}></p>
						            <div className="middle-addelem">
						              <a href={"/"+object.url} className="more_button"> </a>
						            </div>
						          </div>
					        	</div>

					        );
						})}

</div>

		);

	}

}
