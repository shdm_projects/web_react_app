import React, { Component } from 'react';
// import {Link} from 'react-router-dom';
import './Artist.css';

export default class Artist extends Component{

	constructor(props){
		super(props);
		console.log("Artist");
		console.log(props);
		this.state = {
			data: props.data.artist,
			rightMenu: props.data.right_menu
		}
		//console.log(this.state.data);
	}

	render(){

			return (
			<div className="Artist">
				<div className="left col-xs-12 col-sm-12 col-md-9 col-lg-9">
					<div className="artist-contentWrapper">
						<div className="artist_name" dangerouslySetInnerHTML={{ __html: this.state.data.name}}></div>
						<div className="artistItemBlock"><img src={this.state.data.img} className="artist_img" alt=""/></div>
						<div className="artist_description" dangerouslySetInnerHTML={{ __html:this.state.data.description}}></div>
					</div>
				</div>
				{/*<div className="right col-xs-3 col-sm-3 col-md-3 col-lg-3">
					<div>
						<ul className="rightMenuHolder">
						{this.state.rightMenu.map(function(object, i){

							return (<li className="rightMenuItems" key={object.id}><Link className="rightMenuUnit" to={"/"+object.url}>{object.name}</Link></li>);


						})} 

						</ul>
					</div>

					<div className="right_column_block">

						<div className="right_column_line"></div>
						<div className="rightMenuExtendetBlock">{this.state.data.right_box_text}</div>
						<Link to={this.state.data.right_box_url}><div className="more_button"></div></Link>
					</div>

				</div> */}

			</div>
		);

	}

}
