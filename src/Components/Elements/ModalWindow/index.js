import React, { Component } from 'react';
import {Modal, ModalClose, ModalBody} from 'react-modal-bootstrap';
import ImageGallery from 'react-image-gallery';

export default class ModalWindow extends Component {
  constructor(props) {
    super(props);
    var data = {...props.data};
    this.state = {
      data
    }
    console.log(this.state);
  }

  render(){
    var current = this.state.currentVideo;
    return (
      <Modal isOpen={this.state.isOpen} onRequestHide={this.hideModal}>
        <button type="button" className="videos player_button_close" onClick={this.hideModal}>X</button>
        <ModalBody>
          <ImageGallery
                  items={this.state.data.videos}
                  startIndex={current}
                  slideInterval={1000}
                  showPlayButton={false}
                  showFullscreenButton={false}
                  onImageLoad={this.handleImageLoad}
                  onSlide={this._onSlide}
                  renderItem={this._renderVideo}/>
        </ModalBody>
      </Modal>
    );
  }
}
