import React, { Component } from 'react';
import './News.css';
import List from '../List/List';
//import Button from '../Button/Button';
//import CalendarPicker from 'rc-calendar';
import 'rc-calendar/dist/rc-calendar.css';
//import plPL from 'rc-calendar/lib/locale/pl_PL';
import {Link} from 'react-router-dom';

export default class News extends Component{

	constructor(props){
		super(props);

		this.state = {
			lead_pic: props.data.lead_pic?props.data.lead_pic:"",
			data: props.data.news,
			recommended: props.data.recommended?props.data.recommended:new [](),
			button: props.data.button?props.data.button:"",
			r_menu: props.data.right_menu?props.data.right_menu:new [](),
		}
		console.log('news', this.state.data);
	}

	render(){
		var var1 = this.state.data.title?this.state.data.title:"";
		var var2 = this.state.data.image?this.state.data.image:"";
		var var3 = this.state.data.lead?this.state.data.lead:"";


		return (
			<div className="News">
				<div className="left col-xs-9 col-sm-9 col-md-9 col-lg-9">
					<div className="title">
						<div className="article_title artists-pageTitle" dangerouslySetInnerHTML={{ __html: var1 }}></div>
						<div className="article_img_block"><img src={var2} className="article_img" alt=""/></div>
					</div>
					<div className="lead_pic">
						<div className="lead_title" style={{backgroundColor: this.state.lead_pic.box_color}}>{this.state.lead_pic.pic_title}</div>
					  <img src={this.state.lead_pic.image} className="lead_picture" alt="" />
					</div>
					<div className="article_item ">
						<div className="article_lead" dangerouslySetInnerHTML={{ __html: var3 }}></div>
						<div className="article_text" dangerouslySetInnerHTML={{ __html: this.state.data.description }}></div>
					</div>
					<div className="list_item">
						<List data={this.state.recommended} />
					</div>
				</div>
				<div className="right col-xs-3 col-sm-3 col-md-3 col-lg-3">
					<div className="RightMenu">
						<ul className="right_menu_items">
							{this.state.r_menu.map(function(object, i){
								var var4 = object.url?object.url:"";
								var var5 = object.name?object.name:"";
								return(

										<li className="left-menuItem" key={i}><Link className="left-menuUnit" to={"/"+var4}>{var5}</Link></li>

									);
								})
							}

						</ul>
					</div>

				</div>
			</div>
		);

	}

}
