import React, { Component } from 'react';
import {Modal, ModalClose, ModalBody} from 'react-modal-bootstrap';
import ImageGallery from 'react-image-gallery';
import "./index.css";


export default class Images extends Component{

	constructor(props){
		super(props);

		var data = {title: 'zdjęcia', images:[], ...props.data};
		this.state = {
	      open: false,
	      showPlayButton: true,
	      showGalleryPlayButton: false,
	      showFullscreenButton: true,
	      showGalleryFullscreenButton: false,
	      currentImage: 0,
	      player: [],
	      data: data
	    };
	    console.log("Images: ",this.state.data);
		this.openLightbox = this.openLightbox.bind(this);
	    this._renderImages = this._renderImages.bind(this);
	    this._onSlide = this._onSlide.bind(this);
	    this._onReady = this._onReady.bind(this);

	}


	state = {
	    isOpen: false
	  };

	  openModal = (event) => {
	  	console.log(event.target);
	    this.setState({isOpen: true});
	  };

	  openLightbox(index, event){
	        event.preventDefault();
	        this.setState({
	        	isOpen: true,
	        	currentImage: index
	        });

	   };

	  hideModal = () => {
	    this.setState({isOpen: false});
	  };

	  _renderImages(item) {

	    return (
	      <div className='images image-gallery-image'>
	      	<div className='images image-wrapper'>
	      		<img src={item.img} alt='' className='images multimedia_image'/>
						<span className="images image-gallery-description">{item.desc}</span>
	      	</div>
	      </div>
	    );
	  }


	_onReady(event) {
	    const player = this.state.player;
	    player.push(event.target);
	    this.setState({
	      player: player
	    });
	};



	_onSlide() {
	  this.state.data.images.forEach((player) => {

	  });
	}

	handleImageLoad(event) {
		console.log('Image loaded ', event.target)
	}

	render(){

	  	var openLightbox = this.openLightbox;

		return (

				<div className="images row">
					<div className="images col-xs-12 col-sm-12 col-md-12 col-lg-12">
						<div className="images title">{this.state.data.title}</div>
					</div>
					<div className="images col-xs-12 col-sm-12 col-md-12 col-lg-12">

						<div className="images row">
						{
							this.state.data.images.map(function(object, i){
								let backgroundImage = { backgroundImage: 'url(' + object.thumbnail + ')', top: 0, right: 0, left: 0, bottom: 0, position: 'absolute', flex:1, backgroundPosition: 'center', backgroundSize: 'cover', zIndex:1};
								return (
											<div className="images item col-xs-4 col-sm-4 col-md-3 col-lg-3" key={i} >
								        <div className='images multimedia_button' onClick={(e) => openLightbox(i, e)}>
													<div style={backgroundImage}></div>
												</div>
							        </div>
						        );
							})
						}

							<Modal isOpen={this.state.isOpen} onRequestHide={this.hideModal}>
				              <button type="button" className="images player_button_close" onClick={this.hideModal}>X</button>
				              <ModalBody>
				                <ImageGallery
				                        items={this.state.data.images}
				                        startIndex={this.state.currentImage}
				                        slideInterval={2000}
				                        showPlayButton={false}
				                        showFullscreenButton={false}
				                        onImageLoad={this.handleImageLoad}
				                        onSlide={this._onSlide}
				                        renderItem={this._renderImages}/>
				              </ModalBody>
				            </Modal>
				          </div>
					</div>
				</div>

		);

	}
}
