import React, { Component } from 'react';
import {Link} from 'react-router-dom';
import './index.css';
import CalendarPicker from 'rc-calendar';
export default class View_type_3 extends Component{

	constructor(props){
		super(props);

		var data = {list:[], ...props.data};
		this.state = {
			data: data
		}
		console.log("View_type_3: ", this.state);
	}

	render(){

		var object = this.state.data;
		/*
		check whether {object} is an array 
		*/
		if(object.length){
			return (
				<div className="vt3">
			      	<div className="row middle-scrollPoint">
			      	{object.map(function(object, i){
			      		return(
							<div className="list-block">
								<div className="title">{object.header}</div>
								{object.list.map(function(news, index){
									var category = news.category;
									var title = news.title;
									var description = news.description;
									var color = news.color;
									var image = news.image;
									var url = news.url;
									return (
										<div className="wrapped-item" key={index}>
								          <div className="middle-postblock">
								          	<div className="middle-thumbnailholder">
								            	<img  src={image} alt="" />
								              <div className="middlepost-thumbnail"></div>
								              <div className="load-element" style={{backgroundColor: color}}></div>
								            </div>
								            <p className="middlepost-titldesc" dangerouslySetInnerHTML={{ __html: category.toUpperCase() }}></p>
								            <p className="middlepost-titleitem" dangerouslySetInnerHTML={{ __html: title }}></p>
								            <p className="middlepost-description" dangerouslySetInnerHTML={{ __html: description }}></p>
								            <div className="middle-addelem">
								            	<Link className="more_button" to={"/"+url}></Link>
								            </div>
								          </div>
							        	</div>
									);

								})}
							</div>
						)
					})}
					</div>
				</div>
			) 
		}else{
			return (
				<div className="vt3">
			      	<div className="row middle-scrollPoint">
							<div className="list-block">
								<div className="title">{object.header}</div>
								{object.list.map(function(news, index){
									var category = news.category;
									var title = news.title;
									var description = news.description;
									var color = news.color;
									var image = news.image;
									var url = news.url;
									return (
										<div className="wrapped-item" key={index}>
								          <div className="middle-postblock">
								          	<div className="middle-thumbnailholder">
								            	<img  src={image} alt="" />
								              <div className="middlepost-thumbnail"></div>
								              <div className="load-element" style={{backgroundColor: color}}></div>
								            </div>
								            <p className="middlepost-titldesc" dangerouslySetInnerHTML={{ __html: category.toUpperCase() }}></p>
								            <p className="middlepost-titleitem" dangerouslySetInnerHTML={{ __html: title }}></p>
								            <p className="middlepost-description" dangerouslySetInnerHTML={{ __html: description }}></p>
								            <div className="middle-addelem">
								            	<Link className="more_button" to={"/"+url}></Link>
								            </div>
								          </div>
							        	</div>
									);

								})}
							</div>
					</div>
				</div>
			)
		}

	}

}
