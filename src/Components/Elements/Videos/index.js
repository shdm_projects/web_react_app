import React, { Component } from 'react';
import {Modal, ModalClose, ModalBody} from 'react-modal-bootstrap';
import ImageGallery from 'react-image-gallery';
import "react-image-gallery/styles/css/image-gallery.css";
import mask from '../../../assets/images/play_icon.png';
import './index.css';
import YouTube from 'react-youtube';
import ModalWindow from '../ModalWindow';


export default class Videos extends Component{

	constructor(props){
		super(props);
		let data = {title: 'wideos', videos:[], ...props.data};
		this.state = {
	      open: false,
	      showPlayButton: true,
	      showGalleryPlayButton: false,
	      showFullscreenButton: true,
	      showGalleryFullscreenButton: false,
	      currentVideo: 0,
	      player: [],
	      data: data
	    };
			console.log('Video: ', this.state);
			this.openLightbox = this.openLightbox.bind(this);
	    this._renderVideo = this._renderVideo.bind(this);
	    this._onSlide = this._onSlide.bind(this);
	    this._onReady = this._onReady.bind(this);
		}


		state = {
	    isOpen: false
	  };

	  openModal = (event) => {
	  	console.log(event.target);
	    this.setState({isOpen: true});
	  };

	  openLightbox(index, event){
	        event.preventDefault();
					/*console.log(index);
					console.log(event);
					*/
					//this.setState({currentVideo: index});
					//console.log(this.state.currentVideo);
	        this.setState({
	          currentVideo: index,
	          isOpen: true,
	        });
					//this.forceUpdate();
	   };

	  hideModal = (event) => {
			console.log('closes');
	  	/*this.state.player.forEach((player) => {
		  	if (typeof player.pauseVideo === "function") {
					player.pauseVideo();
				}
	  	});*/
	    this.setState({isOpen: false});

	  };

	  closeModal() {
			console.log("closemodal");
	    this.setState({ isOpen: false, player: [] });
	    /*this.state.data.videos.forEach((player) => {
		  	if (typeof player.pauseVideo === "function") {
					player.pauseVideo();
				}
	  	});*/
	  }


	  _renderVideo(item) {
	    const opts = {
	      height: '480',
	      width: '100%',
				playerVars: {
        	controls: 0,
					modestbranding: 1,
					loop: 1
      	}
	    };
	    //console.log(item);
	    return (
	      <div className='videos image-gallery-image'>
	        <div className='videos video-wrapper'>
	          <YouTube
						videoId={item.pathname}
	          opts={opts}
	          onReady={this._onReady}
	          />
	        </div>
	      </div>
	    );
	  }


	   _onReady(event) {
			 //console.log(event);
			 //console.log(event.target);
	    //const player = this.state.player;
	    //player.push(event.target);
	    this.setState({
	      player: event.target
	    });
	  };



	 _onSlide(e) {

		 //this.state.player.stopVideo();
		 //this.setState({player: null});
		// console.log("onslide", this.state.player);
			//this.state.player.pauseVideo();

		 //console.log("slide", this.state.player);
	  /*this.state.player.forEach((player) => {
	  	if (typeof player.pauseVideo === "function") {
			player.pauseVideo();
		}

	  });*/
	}

	 handleImageLoad(event) {
	    console.log('Image loaded ', event.target)
	  }

	  render(){

	  	var openLightbox = this.openLightbox;

		console.log(this.state);
			var current = this.state.currentVideo;
			console.log(current);
		return (
				<div className="videos row">
					<div className="videos col-xs-12 col-sm-12 col-md-12 col-lg-12">
						<div className="videos title">{this.state.data.title}</div>
					</div>
					<div className="videos col-xs-12 col-sm-12 col-md-12 col-lg-12">
						<div className='videos row'>
						{
							this.state.data.videos.map(function(object, i){
								let backgroundImage = { backgroundImage: 'url(' + object.thumbnail + ')', top: 0, right: 0, left: 0, bottom: 0, position: 'absolute', flex:1, backgroundPosition: 'center', backgroundSize: 'cover', zIndex:1};
								return (
									<div className="videos item col-xs-4 col-sm-4 col-md-3 col-lg-3" key={i}>
				            <div className='videos multimedia_button' onClick={(e) => openLightbox(i, e)}>
											<div style={backgroundImage}></div>
											<div className="videos mask">
												<div className="videos mask-img">
													<img src={mask} alt=''/>
												</div>
											</div>
				            </div>
						    	</div>
						    );
							})
						}
				  	</div>
					</div>
					<Modal isOpen={this.state.isOpen} onRequestHide={this.hideModal}>
						<button type="button" className="videos player_button_close" onClick={this.hideModal}>X</button>
						<ModalBody>
							<ImageGallery
											items={this.state.data.videos}
											startIndex={current}
											slideInterval={1000}
											showPlayButton={false}
											showFullscreenButton={false}
											onImageLoad={this.handleImageLoad}
											onSlide={this._onSlide}
											renderItem={this._renderVideo}/>
						</ModalBody>
					</Modal>
				</div>

		);

	}
}
