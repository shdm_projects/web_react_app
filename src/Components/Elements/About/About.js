import React, { Component } from 'react';
import './About.css';
import {Link} from 'react-router-dom';

export default class About extends Component{

	constructor(props){
		super(props);
		console.log(props);
		this.state = {
			data: props.data.article,
			rightMenu: props.data.right_menu,
			config: props.hasOwnProperty('config') ? props.config : []
		}
	}

	/*
	<div className="about-topbanner">
						<div className="about_title artists-pageTitle" dangerouslySetInnerHTML={{ __html: this.state.data.title }}></div>
						<div className="aboutTopWrapper">
							<div className="about_img_block" style={{backgroundImage: "url(" + this.state.data.cover_photo + ")"}}>
								--NEW--<div className="about-topbanner-text" dangerouslySetInnerHTML={{ __html: this.state.data.lead }}></div>
								<div className="about_lead" dangerouslySetInnerHTML={{ __html: this.state.data.lead }}></div>
								<div className="about-colorbox" style={{backgroundColor: '#'+this.state.config.gbcolor}}></div>
							</div>
							
						</div>

					</div>
		*/


	render(){

		var mobile_lead = this.state.data.lead ? <div className="mobile_lead"><div className="mobile_lead_inner" dangerouslySetInnerHTML={{ __html: this.state.data.lead }}></div></div> : null;
		var about_text_style = this.state.data.lead ? {paddingTop: 15} : null;

		return (
			<div className="About">
			<div className="about_title artists-pageTitle" dangerouslySetInnerHTML={{ __html: this.state.data.title }}></div>
				<div className="about-topbanner-box">
						
							<div className="about_img_block" style={{backgroundImage: "url(" + this.state.data.cover_photo + ")"}}>
								<div className="about-colorbox" style={{backgroundColor: '#'+this.state.config.gbcolor}}></div>
							</div>
				</div>

				<div className="about-content left col-xs-12 col-sm-10 col-md-9 col-lg-9">
					<div className="mobileMenu">
						<ul>
							{this.state.rightMenu.map(function(object, i){
								return (<li className="about-grightMenu" key={object.id}><Link to={"/"+object.url}>{object.name}</Link></li>);
							})}
						</ul>
					</div>


					{mobile_lead}
					<div className="about_text" style={about_text_style} dangerouslySetInnerHTML={{ __html: this.state.data.text }}></div>

				</div>
				<div className="right col-xs-2 col-sm-2 col-md-3 col-lg-3">

					<div className="disctopMenuright">
						<ul>
							{this.state.rightMenu.map(function(object, i){

							return (<li className="about-grightMenu" key={object.id}><Link to={"/"+object.url}>{object.name}</Link></li>);
							})}
						</ul>
					</div>


				</div>
			</div>
		);

	}

}
