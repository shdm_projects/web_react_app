import React, { Component } from 'react';
import './List.css';

export default class List extends Component{

	constructor(props){
		super(props);
		console.log(props.data);
		this.state = {
			data: props.data
		}
		console.log("LIST");
		console.log(this.state.data);
	}

	render(){
		return (
			<div className="">
		      	<div className="row middle-scrollPoint">
					{
						this.state.data.map(function(object, i){

var var1 = object.category?object.category:"";
var var2 = object.title?object.title:"";
console.log(object.color);
						return (
								<div className="List col-xs-6 col-sm-4 col-md-4 col-lg-4 text-center" key={i}>
						          <div className="middle-postblock">
						          	<div className="middle-thumbnailholder">
						            	<img  src={object.image} alt="" />
						              <div className="middlepost-thumbnail"></div>
						              <div className="load-element" ></div>
						            </div>
						            <p className="middlepost-titldesc" dangerouslySetInnerHTML={{ __html: var1.toUpperCase() }}></p>
						            <p className="middlepost-titleitem" dangerouslySetInnerHTML={{ __html: var2 }}></p>
						            <p className="middlepost-description" dangerouslySetInnerHTML={{ __html: object.description }}></p>
						            <div className="middle-addelem">
						              <a href={"/"+object.url} className="more_button"> </a>
						            </div>
						          </div>
					        	</div>
					        );
						})
					}
				</div>
		 </div>

		);

	}

}
