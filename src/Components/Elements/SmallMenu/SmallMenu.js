import React, {Component} from 'react';
import {Link} from 'react-router-dom';
import img_logo from '../../../assets/images/Logo.svg';
// import img_element from '../../../assets/images/element_menu.png';
// import img_element_2 from '../../../assets/images/Shop bag.svg';
// import img_element_3 from '../../../assets/images/Search loupe.svg';
// import img_element_4 from '../../../assets/images/White arrow down.svg';
import "./SmallMenu.css";

export default class SmallMenu extends Component {

  constructor(props) {
    super(props);
    
    this.state = {
      data: JSON.parse(props.data.main_menu),
      extend_nav: props.data.hasOwnProperty('extend_nav')?JSON.parse(props.data.extend_nav):[]
    }
    //console.log(this.state.data.data);
  }

  render() {
    return (
      <div className="container SmallMenu">
        <div className="row">

          <div className="col-xs-4 col-sm-3 col-md-2 col-lg-2">
            <img src={img_logo} alt="nifc" className="img-responsive img-logocust"/>
          </div>
          <div className="col-xs-4 col-sm-6 col-md-8 col-lg-8 extraset-item">

            <ul className="mainmenu-list mainmenu-list-colaps">

              {this.state.data.map(function(object, i) {

                return <li className="gb-offset-elem" key={object.id}>
                  <Link to={object.url} className="gb-offset-unit">{object.name}</Link>
                </li>

              })}

            </ul>

          </div>
          <div className="col-xs-3 col-sm-3 col-md-2 col-lg-2">
            <div className="header-extentnav-wrapper" id="">
              <nav className="navbar navbar-default">
                <div className="container-fluid custom-gb-fluid">

                  <div className="navbar-header">
                    <button type="button" className="navbar-toggle collapsed" data-toggle="collapse" data-target="#bs-example-navbar-collapse-1" aria-expanded="false">
                      <span className="sr-only">Toggle navigation</span>
                      <span className="icon-bar"></span>
                      <span className="icon-bar"></span>
                      <span className="icon-bar"></span>
                    </button>

                  </div>

                  <div className="collapse navbar-collapse gb-navbar-collapse" id="bs-example-navbar-collapse-1">
                    <ul className="nav navbar-nav header-extentnav-holder">
                      {this.state.extend_nav.map(function(object, i){

                        return (
                          <li className="header-extendetnav-item">
                            <Link to={object.url} className="navel-unit">
                              <img src={object.img} className="extendet-navigation" alt="nifc"/>
                            </Link>
                          </li>
                        );

                      })}
                    </ul>
                  </div>
                </div>
              </nav>
            </div>
          </div>
        </div>

      </div>

    );

  }

}
