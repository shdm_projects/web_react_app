import React, { Component } from 'react';
// import {Link} from 'react-router-dom';
// import RightMenu from '../../Elements/RightMenu/RightMenu';
import './EditionsList.css';

export default class EditionsList extends Component{

	constructor(props){
		super(props);
		//console.log("Artists");
		//console.log(props);
		this.state = {
			data: props.data.editions
		}
		//console.log(this.state.data);
	}

	render(){

		return (
			<div className="EditionsList">
			<div className="artists-pageTitle"><b>Poprzednie edycje</b></div>
				<div className="row">
					{
						this.state.data.map(function(object, i){

							return (
								<div className="col-xs-6 col-centered col-sm-4 col-md-4 col-lg-4 text-center" key={i}>

						          <div className="middle-postblock">
						          	<div className="middle-thumbnailholder">
						            	<img  src={object.img} alt="" />
						            </div>
						            <p className="middlepost-titldesc">
						              {object.date}
						            </p>
						            <p className="middlepost-titleitem">
						              {object.title}
						            </p>
						            <div className="middle-addelem">
						              <a href={object.url} className="more_button"> </a>
						            </div>
						          </div>

					        	</div>
					        );
						})
					}
				</div>

			</div>
		);

	}

}
