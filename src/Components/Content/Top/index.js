import React, { Component } from 'react';
import BreadCrumbs from '../../Elements/BreadCrumbs/BreadCrumbs';

export default class Top extends Component{

	render(){
		if(this.props.config.breadcrumbs){
			return (
				<section className="content_top">
					<BreadCrumbs breadcrumbs={this.props.breadcrumbs}/>
				</section>
			);
		}else{
			return null;
		}

	}

}
