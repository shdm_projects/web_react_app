import React, { Component } from 'react';

import Top from './Top';
import Middle from './Middle';
import Bottom from './Bottom';
import './Content.css';

export default class Content extends Component{

	constructor(props){
		super(props);
		//console.log(props);
		this.state = {
			data: props.data,
			config: props.config,
			breadcrumbs: props.breadcrumbs
		}
		//console.log(this.state.data.data);
	}

	render(props){
		return (
			<section className="content">
				<div className="">
					<Top config={this.state.config} breadcrumbs={this.state.breadcrumbs}/>
					<Middle data={this.state.data} config={this.state.config}/>
					<Bottom />
				</div>
			</section>
		);

	}

}
