import React, { Component } from 'react';
import CalendarPicker from 'rc-calendar';
import 'rc-calendar/dist/rc-calendar.css';
//import List from '../../Elements/List/List';
import ArtistsList from '../../Elements/ArtistsList/ArtistsList';
import LeftMenu from '../../Elements/LeftMenu/LeftMenu';
//import RightMenu from '../../Elements/RightMenu/RightMenu';
import Artist from '../../Elements/Artist/Artist';
import News from '../../Elements/News/News';
import Calendar from '../../Elements/Calendar/Calendar';
import Article from '../../Elements/Article/Article';
import About from '../../Elements/About/About';
import Multimedia from '../../Elements/Multimedia/Multimedia';
import MultimediaItem from '../../Elements/MultimediaItem/MultimediaItem';
//import EditionsList from '../../Elements/EditionsList/EditionsList';
//import List2 from '../../Elements/List2/List2';
import NewsList from '../../Elements/NewsList/NewsList';
//import Education1 from '../../Elements/Education/Education1';
import ViewType1 from '../../Elements/View_type_1';
import ViewType2 from '../../Elements/View_type_2';
import ViewType3 from '../../Elements/View_type_3';
import ViewType4 from '../../Elements/View_type_4';
import ViewType5 from '../../Elements/View_type_5';
import ViewType6 from '../../Elements/View_type_6';
import ViewType7 from '../../Elements/View_type_7';
import ViewType8 from '../../Elements/View_type_8';
import ViewType9 from '../../Elements/View_type_9';
import ViewType10 from '../../Elements/View_type_10';
import ViewType11 from '../../Elements/View_type_11';

export default class Middle extends Component{

	constructor(props){
		super(props);
		console.log("---CONTENT MIDDLE---");
		console.log("props -> ", props);

		this.state = {
			data: props.data,
			config: props.config

		}

	}

	render(){

		//	var center_element = null;
		console.log("---MAIN---");
		console.log('this.state.config.middle_component -> ',this.state.config.middle_component);
		//var left_menu_data = Array.isArray(this.state.data) ? this.state.data[0] : this.state.data;
		var left_menu_element = <LeftMenu data={Array.isArray(this.state.data) ? this.state.data[0] : this.state.data}/>

		switch(this.state.config.middle_component) {
				case "view_type_1":
				return (
					<section className="content_middle">
						<div className="container">
							<div className="left hidden-xs col-sm-3 col-md-3 col-lg-3">
								{left_menu_element}
							</div>
							<div className="center">
								<ViewType1 data={this.state.data} />
							</div>
						</div>
				 	</section>
				);
				case "view_type_2":
				return (
					<section className="content_middle">
						<div className="container">
							<div className="left hidden-xs col-sm-3 col-md-3 col-lg-3">
								{left_menu_element}
							</div>
							<div className="center">
								<ViewType2 data={this.state.data} />
							</div>
						</div>
				 	</section>
				);
				case "view_type_3":
				return (
					<section className="content_middle">
						<div className="container">
							<div className="left hidden-xs col-sm-3 col-md-3 col-lg-3">
								{left_menu_element}
							</div>
							<div className="center">
								<ViewType3 data={this.state.data} />
							</div>
						</div>
				 	</section>
				);
				case "view_type_4":
				return (
					<section className="content_middle">
						<div className="container">
							<div className="left hidden-xs col-sm-3 col-md-3 col-lg-3">
								{left_menu_element}
							</div>
							<div className="center col-xs-12 col-sm-9 col-md-9 col-lg-9">
								<ViewType4 data={this.state.data} />
							</div>
						</div>
				 	</section>
				);
				case "view_type_5":
				return (
					<section className="content_middle">
						<div className="container">
							<div className="left hidden-xs col-sm-3 col-md-3 col-lg-3">
								{left_menu_element}
							</div>
							<div className="center">
								<ViewType5 data={this.state.data} />
							</div>
						</div>
				 	</section>
				);
				case "view_type_6":
				return (
					<section className="content_middle">
						<div className="container">
							<div className="left hidden-xs col-sm-3 col-md-3 col-lg-3">
								{left_menu_element}
							</div>
							<div className="center">
								<ViewType6 data={this.state.data} />
							</div>
						</div>
				 	</section>
				);
				case "view_type_7":
				return (
					<section className="content_middle">
						<div className="container">
							<div className="left hidden-xs col-sm-3 col-md-3 col-lg-3">
								{left_menu_element}
							</div>
							<div className="center">
								<ViewType7 data={this.state.data} />
							</div>
						</div>
				 	</section>
				);
				case "view_type_8":
				return (
					<section className="content_middle">
						<div className="container">
							<div className="left hidden-xs col-sm-3 col-md-3 col-lg-3">
								{left_menu_element}
							</div>
							<div className="col-xs-12 col-sm-9 col-md-9 col-lg-9">
								<ViewType8 data={this.state.data} />
							</div>
						</div>
				 	</section>
				);
				case "view_type_9":
				return (
					<section className="content_middle">
						<div className="container">
							<div className="left hidden-xs col-sm-3 col-md-3 col-lg-3">
								{left_menu_element}
							</div>
							<div className="center">
								<ViewType9 data={this.state.data} />
							</div>
						</div>
				 	</section>
				);
				case "view_type_10":
				return (
					<section className="content_middle">
						<div className="container">
							<div className="left hidden-xs col-sm-3 col-md-3 col-lg-3">
								{left_menu_element}
							</div>
							<div className="center">
								<ViewType10 data={this.state.data} />
							</div>
						</div>
				 	</section>
				);
				case "view_type_11":
				return (
					<section className="content_middle">
						<div className="container">
							<div className="left hidden-xs col-sm-3 col-md-3 col-lg-3">
								{left_menu_element}
							</div>
							<div className="center">
								<ViewType11 data={this.state.data} />
							</div>
						</div>
				 	</section>
				);
			/*ArtistsList*/
		    case "artists":
		        return (
					<section className="content_middle">
						<div className="container">
							<div className="left hidden-xs col-sm-3 col-md-3 col-lg-3">
								{left_menu_element}
							</div>
							<div className="center">
								<ArtistsList data={this.state.data}/>
							</div>
						</div>
				 	</section>
				);

			/*Multimedia*/
			case "multimedia":
					return (
					<section className="content_middle">
						<div className="container">
							<div className="left hidden-xs col-sm-3 col-md-3 col-lg-3">
								{left_menu_element}
							</div>
							<div className="center col-xs-9 col-sm-9 col-md-9 col-lg-9 ">
								<Multimedia data={this.state.data} />
							</div>
						</div>
				 	</section>
				);
			/*Multimedia item*/
			case "multimedia_item":
				return (
					<section className="content_middle">
						<div className="container">
							<div className="left hidden-xs col-sm-3 col-md-3 col-lg-3">
								{left_menu_element}
							</div>
							<div className="center col-xs-9 col-sm-9 col-md-9 col-lg-9">
								<MultimediaItem data={this.state.data} />
							</div>
						</div>
				 	</section>
				);
			/*Article*/
			case "article":
				return (
					<section className="content_middle">
						<div className="container">
							<div className="left hidden-xs col-sm-3 col-md-3 col-lg-3">
								{left_menu_element}
							</div>
							<div className="center col-xs-9 col-sm-9 col-md-9 col-lg-9">
								<Article data={this.state.data} />
							</div>
						</div>
				 	</section>
				);
				/*O festiwalu ABOUT*/
				case "about":
					return (
						<section className="content_middle">
							<div className="container aboutcont-wrapper">
								<div className="left hidden-xs col-sm-12 col-md-3 col-lg-3">
									{left_menu_element}
								</div>
								<div className="col-xs-12 col-sm-12 col-md-9 col-lg-9 extended-box">
									<About data={this.state.data} config={this.state.config}/>
								</div>
							</div>
					 	</section>
					);

				case "news_list":
					return(
						<section className="content_middle">
							<div className="container">
								<div className="center col-xs-12 col-sm-12 col-md-12 col-lg-12">
									<NewsList data={this.state.data} />
								</div>
							</div>
					 	</section>
					);
				case "newslist":
					return(
						<section className="content_middle">
							<div className="container">
								<div className="center col-xs-12 col-sm-12 col-md-12 col-lg-12">
									<NewsList data={this.state.data} />
								</div>
							</div>
					 	</section>
					);
				case "calendar":
					return (
						<section className="content_middle">
							<div className="container">
								<div className="hidden-xs col-sm-3 col-md-3 col-lg-3">
									{left_menu_element}
								</div>
								<div className="col-xs-6 col-sm-6 col-md-6 col-lg-6">
									<Calendar data={this.state.data} config={this.state.config}/>
								</div>
								<div className="right col-xs-3 col-sm-3 col-md-3 col-lg-3">
									<CalendarPicker />
								</div>
							</div>
					 	</section>
					);
					/*Artist*/
				case "artist":
				return (
					<section className="content_middle">
						<div className="container">
							<div className="hidden-xs col-sm-12 col-md-3 col-lg-3">
								{left_menu_element}
							</div>
							<div className="col-xs-12 col-sm-12 col-md-9 col-lg-9">
								<Artist data={this.state.data}/>
							</div>
						</div>
				 	</section>
				);

				/*News*/
				case "news":
					return (
						<section className="content_middle">
							<div className="container">
								<div className="left hidden-xs col-sm-3 col-md-3 col-lg-3">
									{left_menu_element}
								</div>
								<div className="center col-xs-9 col-sm-9 col-md-9 col-lg-9">
									<News data={this.state.data} />
								</div>
							</div>
					 	</section>
					);
				/*Sponsors*/
				case "sponsors":
					return (
						<section className="content_middle">
							<div className="container">
								<div className="left col-xs-3 col-sm-3 col-md-3 col-lg-3">
									{left_menu_element}
								</div>
								<div className="center col-xs-9 col-sm-9 col-md-9 col-lg-9">
									 <Article data={this.state.data} />
								</div>
							</div>
						</section>
					);
			/*Artist*/
			/*case "artist":
				return (
					<section className="content_middle">
						<div className="container">
							<div className="hidden-xs col-sm-12 col-md-3 col-lg-3">
								<LeftMenu data={this.state.data}/>
							</div>
							<div className="col-xs-12 col-sm-12 col-md-9 col-lg-9">
								<Artist data={this.state.data}/>
							</div>
						</div>
				 	</section>
				);
			/*Calendar*/
			/*case "calendar":
				return (
					<section className="content_middle">
						<div className="container">
							<div className="hidden-xs col-sm-3 col-md-3 col-lg-3">
								<LeftMenu data={this.state.data}/></div>
							<div className="col-xs-6 col-sm-6 col-md-6 col-lg-6">
								<Calendar data={this.state.data} config={this.state.config}/>
							</div>
							<div className="right col-xs-3 col-sm-3 col-md-3 col-lg-3">
								<CalendarPicker />
							</div>
						</div>
				 	</section>
				);

			/*EditionsList*/
			/*case "editions_list":
		        return (
					<section className="content_middle">
						<div className="container">
							<div className="left hidden-xs col-sm-3 col-md-3 col-lg-3">
								<LeftMenu data={this.state.data}/>
							</div>
							<div className="center col-xs-9 col-sm-9 col-md-9 col-lg-9">
								<EditionsList data={this.state.data}/>
							</div>
						</div>
				 	</section>
				);*/

				/*catalog_main*/
				/*case "catalog_main":
			        return (
						<section className="content_middle">
							<div className="container">
								<div className="left hidden-xs col-sm-3 col-md-3 col-lg-3">
									<LeftMenu data={this.state.data}/>
								</div>
								<div className="center col-xs-6 col-sm-6 col-md-6 col-lg-6">
									<List2 data={this.state.data}/>
								</div>
								<div className="right col-xs-3 col-sm-3 col-md-3 col-lg-3">
									<RightMenu data={this.state.data}/>
									<CalendarPicker />
								</div>
							</div>
					 	</section>
					);


					case "collection-history":
				        return (
							<section className="content_middle">
								<div className="container">
									<div className="left hidden-xs col-sm-3 col-md-3 col-lg-3">
										<LeftMenu data={this.state.data}/>
									</div>
									<div className="center col-xs-6 col-sm-6 col-md-6 col-lg-6">
										<News data={this.state.data}/>
									</div>
									<div className="right col-xs-3 col-sm-3 col-md-3 col-lg-3">

									</div>
								</div>
						 	</section>
						);

					case "collection-online":
								return (
							<section className="content_middle">
								<div className="container">
									<div className="left hidden-xs-2 col-sm-2 col-md-2 col-lg-2">
										<LeftMenu data={this.state.data}/>
									</div>
									<div className="center col-xs-10 col-sm-10 col-md-10 col-lg-10">
										<News data={this.state.data}/>
									</div>
								</div>
							</section>
						);*/





		    default:
		    	return (
					<section className="content_middle">
						<div className="container">
							<div className="center col-xs-12 col-sm-12 col-md-12 col-lg-12">
								<NewsList data={this.state.data} />
							</div>
						</div>
				 	</section>
				);

		}

	}

}
