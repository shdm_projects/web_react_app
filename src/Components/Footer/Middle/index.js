import React, { Component } from 'react';
import img_logo from '../../../assets/images/Logo - color.svg';
import {Link} from 'react-router-dom';


export default class Middle extends Component{

	constructor(props){
		super(props);

		this.state = {
			data: props.data,
			config: props.config
		}

	}

	render(){

		var list = this.state.data;

		var arrayList = [];

		Object.keys(this.state.data).map(function(key,value) {

			arrayList.push(list[key]);
			return 0;

		});

		return (
			<section className="footer_middle">
				<div className="container">
					<div className="row footer-extend">
						<div className="col-xs-5 col-sm-2 col-md-2 col-lg-2 text-center">
							<img className="img-responsive gb-img-logocust-footer" src={img_logo} alt="" />
						</div>
						<div className="col-xs-5 col-sm-10 col-md-10 col-lg-10 text-center">
						{Object.keys(this.state.data).map(function(key,value) {

								return (
									<div className="" key={key}>
										<div className="gb-footer-menu text-center">
											<div className="footer-infoblock">
													{Object.keys(arrayList[value]).map(function(key1,value1) {

														if(key1 === "name"){
															return (
																<p className="fr-inf-title" key={value1}>
																	{arrayList[value].name}
																</p>
															);

														}
														return null;
													})}

												<ul className="fr-inflsholder list-footer-middle">
													{Object.keys(arrayList[value]).map(function(key1,value1) {

														if(key1 !== "name"){

															return (<li className="fr-inflitem" key={value1}><Link to={arrayList[value][key1].url} className="fr-influnit">{arrayList[value][key1].name}</Link></li>);

														}
														return null;
													})}
												</ul>
											</div>
										</div>
									</div>

								);
						})}

					</div>
					</div>
				</div>
			</section>

		);

	}

}
