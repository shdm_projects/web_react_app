import Top from './Top';
import Middle from './Middle';
import Bottom from './Bottom';
import React, { Component } from 'react';
import './Footer.css';



export default class Footer extends Component{

  constructor(props){
    super(props);

    this.state = {
      data: props.data,
      config: props.config
    }


  }

  render(props){

    /*const style = {

      backgroundColor:"#"+this.state.config.gbcolor

    };*/

    return (
      <section className="footer">
            <Top/>
            <Middle data={this.state.data} config={this.state.config}/>
            <Bottom/> 
      </section>
    );

  }

}


  
