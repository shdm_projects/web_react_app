import React, {Component} from 'react';
import facebook from '../../../assets/images/facebook.svg';
import twitter from '../../../assets/images/twitter.svg';
import youtube from '../../../assets/images/youtube.svg';


export default class Bottom extends Component {

  constructor(props) {
    super(props);

    this.state = {
      data: props.data,
      config: props.config
    }

  }

  render() {

    return (
      <section className="footer_bottom">
        <div className="footerinfo-block">
          <div className="container">
            <div className="row">
              <div className="footerinfobl-wrap">
                <div className="col-xs-12 col-sm-6 col-md-6 col-lg-6">
                  <p className="ftinfo-bl-copyright">
                    &copy; Narodowy Instytut Fryderyka Chopina 2003-2018
                  </p>
                </div>

                <div className="col-xs-12 col-sm-6 col-md-6 col-lg-6">
                  <div className="footersocila-wrapper">
                    <ul className="fr-inflsholder">
                      <li className="footer-socilamedia">
                        <a href="https://www.facebook.com/Narodowy-Instytut-Fryderyka-Chopina-1570915943228919/">
                          <img src={facebook} alt="facebook" /></a>
                        </li>
                        <li className="footer-socilamedia">
                          <a href="https://twitter.com/chopininstitute">
                            <img src={twitter} alt="twitter" /></a>
                          </li>
                          <li className="footer-socilamedia">
                            <a href="https://www.youtube.com/channel/UCSTXol20Q01Uj-U5Yp3IqFg">
                              <img src={youtube} alt="youtube" /></a>
                            </li>
                          </ul>
                        </div>
                      </div>

                    </div>
                  </div>
                </div>
              </div>
            </section>
					);
				}
			}
