import React from 'react';
import ReactDOM from 'react-dom';
import App from './App';
//import registerServiceWorker from './registerServiceWorker';
import {BrowserRouter, Switch, Redirect, Route} from "react-router-dom";

ReactDOM.render(
	<BrowserRouter>
	<Switch>
	  <Redirect from='//' to='/pl'/>
	  <Route path='/pl' component={App}/>
	  <Route path='/pl/*' component={App}/>
	  <Route path='/en' component={App}/>
	  <Route path='/en/*' component={App}/>
	  <Redirect from='*' to='/pl/'/>
	</Switch>
	</BrowserRouter>
  ,
   document.getElementById('root')
   );
//registerServiceWorker();
