import React, { Component } from 'react';

import Builder from './Components/Builder/Builder';

class App extends Component {

  render() {

    return (

        <Builder data={this.props} />

    );
  }
}


export default App;
